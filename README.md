# The RHEDcloud Lightweight Directory Service

##Copyright

This software was created at Emory University. Please see NOTICE.txt for Emory's copyright and disclaimer notifications. This software was extended and modified by the RHEDcloud Foundation and is copyright 2019 by the RHEDcloud Foundation.

##License

This software is licensed under the Apache 2.0 license included in this distribution as LICENSE-2.0.txt

##Description

A Lightweight Directory Service ESB Service (LDSS) to participate in its AWS account provisioning orchestration.
LDSS handles query, create, update, and delete requests for groups to create groups that implement
roles for Emory’s Identity Management System (IDM).
