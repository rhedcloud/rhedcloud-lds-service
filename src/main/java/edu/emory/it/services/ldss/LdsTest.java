package edu.emory.it.services.ldss;

import edu.emory.it.services.ldss.provider.DirectorySearchLDS;
import edu.emory.it.services.ldss.provider.LdsProviderUtil;
import edu.emory.it.services.ldss.provider.ProviderException;
import edu.emory.moa.jmsobjects.lightweightdirectoryservices.v1_0.Group;
import edu.emory.moa.jmsobjects.lightweightdirectoryservices.v1_0.OrganizationalUnit;
import org.apache.directory.api.ldap.model.constants.LdapConstants;
import org.apache.directory.api.ldap.model.cursor.EntryCursor;
import org.apache.directory.api.ldap.model.entry.DefaultEntry;
import org.apache.directory.api.ldap.model.entry.Entry;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.api.ldap.model.message.SearchScope;
import org.apache.directory.api.ldap.model.name.Dn;
import org.apache.directory.ldap.client.api.LdapConnection;
import org.apache.directory.ldap.client.api.LdapConnectionConfig;
import org.apache.directory.ldap.client.api.LdapNetworkConnection;
import org.apache.directory.ldap.client.api.NoVerificationTrustManager;
import org.openeai.config.EnterpriseFieldException;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import javax.naming.ldap.StartTlsRequest;
import javax.naming.ldap.StartTlsResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class LdsTest {
    enum OP {q, qp, qj, c, C, d, u, dsbase, dsbasic}

    public static void main(String[] args) {
        OP arg = (args.length > 0) ? OP.valueOf(args[0]) : OP.q; // query by default

        switch (arg) {
            case q:
                query();
                break;
            case qp:
                queryPool();
                break;
            case qj:
                queryJndi();
                break;
            case c:
                simpleCreate();
                query();
                break;
            case C:
                fullCreate();
                query();
                break;
            case d:
                delete();
                query();
                break;
            case u:
                update();
                query();
                break;
            case dsbase:
                directorySearchBase();
                break;
            case dsbasic:
                directorySearchBasic();
                break;
        }
    }

    private static void query() {
        LdapConnectionConfig ldapConfig = newLdapConnectionConfig();

        try (LdapNetworkConnection connection = new LdapNetworkConnection(ldapConfig)) {
            if (!connection.connect()) {
                throw new RuntimeException("Unable to connect");
            }
            connection.startTls();
            connection.bind();

            // for all OUs at that level
            // cursor = connection.search("DC=emory,DC=edu", LdapConstants.OBJECT_CLASS_STAR, SearchScope.SUBTREE);

            // some valid AWS OUs:
            // 073387691481
            // 134285882893
            // 134285882893 has EmoryAdministratorRol, EmoryAuditorRole, EmoryCentralAdministratorRole

            // lookup uses SearchScope.OBJECT
            Entry entl = connection.lookup(new Dn("OU=073387691481,OU=AWS,DC=emory,DC=edu"));
            System.out.println((entl == null) ? "lookup returned null" : entl);


            EntryCursor cursor;

            cursor = connection.search("OU=134285882893,OU=AWS,DC=emory,DC=edu", LdapConstants.OBJECT_CLASS_STAR, SearchScope.SUBTREE);
            for (Entry ent : cursor) {
                System.out.println(ent);
            }

            cursor = connection.search("OU=999999999999,OU=AWS,DC=emory,DC=edu", LdapConstants.OBJECT_CLASS_STAR, SearchScope.SUBTREE);
            for (Entry ent : cursor) {
                System.out.println(ent);
            }
        } catch (Exception e) {
            // see fullCreate() for detailed exception info
            e.printStackTrace();
        }
    }

    private static void queryPool() {
        LdsProviderUtil.initLdapPool("ldsautheudev.service.emory.edu", 389, "CN=esb_group_svc,OU=Services,DC=emory,DC=edu", "See LdsService-dev.xml");

//        String dn = "OU=134285882893,OU=AWS,DC=emory,DC=edu";
        String dn = "CN=EmoryAdministratorRole,OU=134285882893,OU=AWS,DC=emory,DC=edu";
        LdapConnection connection = null;
        try {
            connection = LdsProviderUtil.getLdapConnection();
            //connection.setSchemaManager(new DefaultSchemaManager());

            EntryCursor cursor = connection.search(dn, LdapConstants.OBJECT_CLASS_STAR, SearchScope.OBJECT,
                    LdsProviderUtil.returningAttributes);
            for (Entry ent : cursor) {
                System.out.println(ent);
                System.out.println();
                System.out.println();

                OrganizationalUnit ou;
                Group group;

                if (ent.hasObjectClass("organizationalUnit")) {
                    ou = LdsProviderUtil.populateOrganizationalUnit(new OrganizationalUnit(), ent);
                    System.out.println(ou);
                } else if (ent.hasObjectClass("group")) {
                    group = LdsProviderUtil.populateGroup(new Group(), ent);
                    System.out.println(group);
                } else {
                    throw new ProviderException("Unknown Entry objectClass: " + ent.get("objectClass"));
                }
            }
        } catch (LdapException | EnterpriseFieldException | ProviderException e) {
            e.printStackTrace();
        } finally {
            LdsProviderUtil.releaseLdapConnection(connection);
            LdsProviderUtil.closeLdapPool();
        }
    }

    private static void queryJndi() {
        // see http://www.adamretter.org.uk/blog/entries/LDAPTest.java
        // http://www.java2s.com/Code/Java/JNDI-LDAP/LDAPSearch.htm
        // https://gist.github.com/jbarber/2909828
        // https://www.javaworld.com/article/2076073/java-web-development/ldap-and-jndi--together-forever.html


        // https://docs.oracle.com/javase/jndi/tutorial/ldap/ext/starttls.html
        // https://stackoverflow.com/questions/38013340/retrieving-operational-internal-ldap-attributes-via-jndi-in-one-batch

        // Set up environment for creating initial context
        Hashtable<Object, Object> env = new Hashtable<>(11);
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        // Must use the name of the server that is found in its certificate
        env.put(Context.PROVIDER_URL, "ldap://ldsautheudev.service.emory.edu:389/");

        // the following is helpful in debugging errors
        env.put("com.sun.jndi.ldap.trace.ber", System.err);


        LdapContext ctx = null;
        StartTlsResponse tls = null;
        try {
            // Create initial context and start TLS
            ctx = new InitialLdapContext(env, null);
            tls = (StartTlsResponse) ctx.extendedOperation(new StartTlsRequest());
            tls.negotiate();

            ctx.addToEnvironment(Context.SECURITY_AUTHENTICATION, "simple");
            ctx.addToEnvironment(Context.SECURITY_PRINCIPAL, "CN=esb_group_svc,OU=Services,DC=emory,DC=edu");
            ctx.addToEnvironment(Context.SECURITY_CREDENTIALS, "See LdsService-dev.xml");

            //System.out.println(ctx.getAttributes(""));

//            String[] returningAttributes = {"name", "instanceType", "uSNCreated", "objectClass", "structuralObjectClass"};
            String[] returningAttributes = {"*", "+"};

            SearchControls controls = new SearchControls();
            controls.setSearchScope(SearchControls.OBJECT_SCOPE);
            controls.setReturningAttributes(returningAttributes);
            NamingEnumeration<SearchResult> results = ctx.search("OU=134285882893,OU=AWS,DC=emory,DC=edu",
                    "(objectClass=organizationalUnit)", controls);
            while (results.hasMore()) {
                SearchResult searchResult = results.next();
                Attributes attributes = searchResult.getAttributes();
                NamingEnumeration<?> atts = attributes.getAll();
                while (atts.hasMore()) {
                    Object jk = atts.next();
                    BasicAttribute a = (BasicAttribute) jk;
                    if ("objectGUID".equals(a.getID())) {
                        UUID guid = UUID.nameUUIDFromBytes(String.valueOf(a.get()).getBytes(StandardCharsets.UTF_8));
                        System.out.println("objectGUID: " + guid.toString());
                    } else {
                        System.out.println(jk);
                    }
                }
                System.out.println();
            }

        } catch (NamingException | IOException e) {
            e.printStackTrace();
        } finally {
            try {
                // Stop TLS and close the context when we're done
                if (tls != null) tls.close();
                if (ctx != null) ctx.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static void simpleCreate() {
        LdapConnectionConfig ldapConfig = newLdapConnectionConfig();

        try (LdapNetworkConnection connection = new LdapNetworkConnection(ldapConfig)) {
            if (!connection.connect()) {
                throw new RuntimeException("Unable to connect");
            }
            connection.startTls();
            connection.bind();

            String id = "999999999999";

            connection.add(
                    new DefaultEntry(
                              "OU=" + id + ",OU=AWS,DC=emory,DC=edu" // The Dn
                            , "ObjectClass: top"
                            , "ObjectClass: organizationalUnit"
                            , "Description: Kevin Hale Boyes Test OU(" + id + ")"
                    ));
        } catch (Exception e) {
            // see fullCreate() for detailed exception info
            e.printStackTrace();
        }
    }

    private static void fullCreate() {
        LdapConnectionConfig ldapConfig = newLdapConnectionConfig();
        List<Entry> addedEntries = new ArrayList<>();  // in case of error and need to roll back partial additions
        List<Entry> entriesToAdd = new ArrayList<>();

        try (LdapNetworkConnection connection = new LdapNetworkConnection(ldapConfig)) {
            if (!connection.connect()) {
                throw new RuntimeException("Unable to connect");
            }
            connection.startTls();
            connection.bind();

            String id = "999999999999";

            Dn awsDn = new Dn("OU=" + id + ",OU=AWS,DC=emory,DC=edu");
            Entry awsEntry = new DefaultEntry(
                    awsDn
                    , "ObjectClass: organizationalUnit"
                    , "ObjectClass: top"
                    , "Description: KHB-TEST - Amazon AWS OU(" + id + ") in IDM for Amazon Web Services"
            );
            /* private static final */ Dn administratorRole = new Dn("CN=EmoryAdministratorRole");
            Entry administratorGroupEntry = new DefaultEntry(
                    awsDn.add(administratorRole)
                    , "ObjectClass: group"
                    , "ObjectClass: top"
                    , "Description: EmoryAdministratorRole Group for IDM for Amazon Web Services"
            );
            Dn auditorRole = new Dn("CN=EmoryAuditorRole");
            Entry auditorGroupEntry = new DefaultEntry(
                    awsDn.add(auditorRole)
                    , "ObjectClass: group"
                    , "ObjectClass: top"
                    , "Description: EmoryAuditorRole Group for IDM for Amazon Web Services"
            );
            Dn centralAdministratorRole = new Dn("CN=EmoryCentralAdministratorRole");
            Entry centralAdministratorGroupEntry = new DefaultEntry(
                    awsDn.add(centralAdministratorRole)
                    , "ObjectClass: group"
                    , "ObjectClass: top"
                    , "Description: EmoryCentralAdministratorRole Group for IDM for Amazon Web Services"
            );

            entriesToAdd.add(awsEntry);
            entriesToAdd.add(administratorGroupEntry);
            entriesToAdd.add(auditorGroupEntry);
            entriesToAdd.add(centralAdministratorGroupEntry);

            Exception rollbackNeeded = null;
            for (Entry ent : entriesToAdd) {
                try {
                    connection.add(ent);
                    addedEntries.add(ent);
                } catch (Exception e) {
                    rollbackNeeded = e;
                    break;
                }
            }
            if (rollbackNeeded != null) {
                for (Entry ent : addedEntries) {
                    try {
                        connection.delete(ent.getDn());
                    } catch (Exception e) {
                        // errors while rolling back changes suck
                        break;
                    }
                }
            }
        } catch (Exception e) {
            // IOException from LdapNetworkConnection
            // LdapException from connection operations
            // LdapEntryAlreadyExistsException (et.al.) are subclasses of LdapException
            e.printStackTrace();
        }
    }
    private static void delete() {
        LdapConnectionConfig ldapConfig = newLdapConnectionConfig();

        try (LdapNetworkConnection connection = new LdapNetworkConnection(ldapConfig)) {
            if (!connection.connect()) {
                throw new RuntimeException("Unable to connect");
            }
            connection.startTls();
            connection.bind();

            // if groups exist then a simple delete() will result in LdapContextNotEmptyException
            connection.deleteTree("OU=999999999999,OU=AWS,DC=emory,DC=edu");

        } catch (Exception e) {
            // see fullCreate() for detailed exception info
            // plus
            // LdapNoSuchObjectException is interesting
            e.printStackTrace();
        }
    }

    private static void update() {
        LdapConnectionConfig ldapConfig = newLdapConnectionConfig();

        try (LdapNetworkConnection connection = new LdapNetworkConnection(ldapConfig)) {
            if (!connection.connect()) {
                throw new RuntimeException("Unable to connect");
            }
            connection.startTls();
            connection.bind();

            // http://directory.apache.org/api/user-guide/2.6-modifying.html
//            connection.modify()

        } catch (Exception e) {
            // see fullCreate() for detailed exception info
            e.printStackTrace();
        }
    }

    private static LdapConnectionConfig newLdapConnectionConfig() {
        LdapConnectionConfig ldapConfig = new LdapConnectionConfig();
        ldapConfig.setLdapHost("ldsautheudev.service.emory.edu");
        ldapConfig.setLdapPort(389);
        // https://www.programcreek.com/java-api-examples/index.php?api=org.apache.directory.ldap.client.api.LdapConnectionConfig
        // https://www.programcreek.com/java-api-examples/javax.net.ssl.TrustManager
        ldapConfig.setTrustManagers(new NoVerificationTrustManager());
        ldapConfig.setName("CN=esb_group_svc,OU=Services,DC=emory,DC=edu");
        ldapConfig.setCredentials("See LdsService-dev.xml");
        return ldapConfig;
    }

    private static final String RHEDCLOUD_SECURITY_CREDENTIALS = "See LdsService-dev.xml";
    private static final String EMORY_SECURITY_CREDENTIALS = "See LdsService-dev.xml";
    private static final String RICE_SECURITY_CREDENTIALS = "See LdsService-dev.xml";

    private static final String searchString = "hale*boyes";

    // RHEDcloud
    // full list of attributes
    // cn=khboyes: null:null:{givenname=givenName: Kevin,
    //                        sn=sn: Hale Boyes,
    //                        userpassword=userPassword: [B@52a86356,
    //                        uidnumber=uidNumber: 2005,
    //                        gidnumber=gidNumber: 100,
    //                        objectclass=objectClass: inetOrgPerson, posixAccount, top,
    //                        uid=uid: khboyes,
    //                        cn=cn: khboyes,
    //                        homedirectory=homeDirectory: /home/users/khboyes}

    private static final String rhedcloudReturningAttributesCsv = "cn,givenName,sn,uid";
    private static final String rhedcloudSearchBase = "ou=Users,dc=rhedcloud,dc=org";
    private static final String rhedcloudSortKey = "sn,givenName";
    private static final String rhedcloudBasicSearchAttributes = "givenName,sn,uid";
    private static final String rhedcloudPrimaryKey = "uid";
    private static final String rhedcloudSearchFilter
            = "(|"
            + "(givenName=*" + searchString + "*)"
            + "(sn=*" + searchString + "*)"
            + "(uid=*" + searchString + "*)"
            + ")";

    // Emory
    // partial list of attributes
    // CN=kboyes,OU=People: null:null:{emorygivenname=emoryGivenName: Kevin,
    //                                 fullname=fullName: Kevin Hale Boyes,
    //                                 mail=mail: kboyes@emory.edu,
    //                                 publishexternal=publishexternal: Y,
    //                                 serialnumber=serialNumber: P4883103,
    //                                 publishinternal=publishinternal: Y,
    //                                 sn=sn: Hale Boyes,
    //                                 organizationalstatus=organizationalStatus: Sponsored}
    // full list of attributes
    // CN=kboyes,OU=People: null:null:{emorygivenname=emoryGivenName: Kevin,
    //                                 fullname=fullName: Kevin Hale Boyes,
    //                                 mail=mail: kboyes@emory.edu,
    //                                 publishexternal=publishexternal: Y,
    //                                 serialnumber=serialNumber: P4883103,
    //                                 publishinternal=publishinternal: Y,
    //                                 sn=sn: Hale Boyes,
    //                                 organizationalstatus=organizationalStatus: Sponsored,
    //                                 //
    //                                 givenname=givenName: Kevin,
    //                                 displayname=displayName: Kevin Hale Boyes,
    //                                 suppressinfo=suppressinfo: N,
    //                                 uid=uid: kboyes,
    //                                 cn=cn: kboyes,
    //                                 distinguishedname=distinguishedName: CN=kboyes,OU=People,DC=emory,DC=edu,
    //                                 objectclass=objectClass: top, mailRecipient, shadowAccount, eduPerson, posixAccount, person, organizationalPerson, inetOrgPerson, newPilotPerson, emoryPerson, emoryProxyUser,
    //                                 companyaccount=companyAccount: EUV,
    //                                 uidlower=uidLower: kboyes}
    private static final String emoryReturningAttributesCsv
            = "serialNumber,fullName,emoryGivenName,sn,mail,postalAddress,telephoneNumber"
            + ",facsimileTelephoneNumber,publishexternal,publishinternal,organizationalStatus"
            + ",ou,businessCategory,title,emorystudentphone,physicalDeliveryOfficeName,FirstMiddle,Suffix"
            + ",emoryGivenName,generationQualifier";
    private static final String emorySearchBase = "DC=emory,DC=edu";
    private static final String emorySortKey = "sn,emoryGivenName";
    private static final String emoryBasicSearchAttributes = "fullName,mail,serialNumber,sn,telephoneNumber,uid";
    private static final String emoryPrimaryKey = "serialNumber";
    private static final String emorySearchFilter
            = "(|"
            + "(fullName=*" + searchString + "*)"
            + "(mail=" + searchString + "*)"
            + "(serialNumber=" + searchString + "*)"
            + "(sn=*" + searchString + "*)"
            + "(telephoneNumber=*" + searchString + "*)"
            + "(uid=" + searchString + "*)"
            + ")";

    // Rice
    private static final String riceReturningAttributesCsv
            = "givenName,mail,physicalDeliveryOfficeName"
            + ",postalAddress,riceCollege,riceCurriculum,riceDisplayPhone,ricePermPhone"
            + ",sn,telephoneNumber,title,uid";
    private static final String riceSearchBase = "ou=People,dc=rice,dc=edu";
    private static final String riceSortKey = "sn,givenName";
    private static final String riceBasicSearchAttributes
            = "eduPersonPrimaryAffiliation,givenName,mail,physicalDeliveryOfficeName"
            + ",postalAddress,riceCollege,riceCurriculum,riceDisplayPhone,ricePermPhone"
            + ",sn,telephoneNumber,title,uid";
    private static final String ricePrimaryKey = "uid";
    private static final String riceSearchFilter
            = "(|"
            + "(eduPersonPrimaryAffiliation=*" + searchString + "*)"
            + "(givenName=*" + searchString + "*)"
            + "(mail=*" + searchString + "*)"
            + "(givenName=*" + searchString + "*)"
            + "(physicalDeliveryOfficeName=*" + searchString + "*)"
            + "(postalAddress=*" + searchString + "*)"
            + "(riceCollege=*" + searchString + "*)"
            + "(riceCurriculum=*" + searchString + "*)"
            + "(riceDisplayPhone=*" + searchString + "*)"
            + "(ricePermPhone=*" + searchString + "*)"
            + "(sn=*" + searchString + "*)"
            + "(telephoneNumber=*" + searchString + "*)"
            + "(title=*" + searchString + "*)"
            + "(uid=*" + searchString + "*)"
            + ")";

    private static void directorySearchBase() {
        try {
            NamingEnumeration<SearchResult> results;
            results = DirectorySearchLDS.search(getDirContextRHEDcloud(), rhedcloudSearchBase, rhedcloudSearchFilter, "sub", rhedcloudReturningAttributesCsv, 1000);
//            results = DirectorySearchLDS.search(getDirContextEmory(), emorySearchBase, emorySearchFilter, "sub", emoryReturningAttributesCsv, 1000);
//            results = DirectorySearchLDS.search(getDirContextRice(), riceSearchBase, riceSearchFilter, "sub", riceReturningAttributesCsv, 1000);

            while (results.hasMore()) {
                // each search result contains name, object, class name, and Attributes
                SearchResult sr = results.next();
                System.out.println(sr);
            }
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    private static void directorySearchBasic() {
        // RHEDcloud
        DirectorySearchLDS srch = new DirectorySearchLDS(getDirContextRHEDcloud());
        srch.setLdapSearchBase(rhedcloudSearchBase);
        srch.setReturningAttributes(rhedcloudReturningAttributesCsv);
        srch.setPrimaryKey(rhedcloudPrimaryKey);
        srch.setSortKeys(rhedcloudSortKey);
        srch.setBasicSearchAttributes(rhedcloudBasicSearchAttributes);
        // Emory
//        DirectorySearchLDS srch = new DirectorySearchLDS(getDirContextEmory());
//        srch.setLdapSearchBase(emorySearchBase);
//        srch.setReturningAttributes(emoryReturningAttributesCsv);
//        srch.setPrimaryKey(emoryPrimaryKey);
//        srch.setSortKeys(emorySortKey);
//        srch.setBasicSearchAttributes(emoryBasicSearchAttributes);
//        srch.setSearchRestriction("(publishInternal=Y)");
        // Rice
//        DirectorySearchLDS srch = new DirectorySearchLDS(getDirContextRice());
//        srch.setLdapSearchBase(riceSearchBase);
//        srch.setReturningAttributes(riceReturningAttributesCsv);
//        srch.setPrimaryKey(ricePrimaryKey);
//        srch.setSortKeys(riceSortKey);
//        srch.setBasicSearchAttributes(riceBasicSearchAttributes);
        // the service account has access to inactive entries because there are
        // times when a person is formally inactive but still needs access to things
        //srch.setSearchRestriction("(riceuserstatus=active)");


        List<Map<String, String>> resultMapList;
        long start = System.currentTimeMillis();
        resultMapList = srch.doBasic("hale boyes");
        long stop = System.currentTimeMillis();
        System.out.println("doBasic took " + (stop - start) + " ms");

        if (srch.getErrorCode() == DirectorySearchLDS.NO_MATCHES) {
            System.out.println(srch.getErrorMessage() + " (" + srch.getErrorCode() + ")");
        }
        else if (srch.getErrorCode() == DirectorySearchLDS.TOO_MANY) {
            System.out.println(srch.getErrorMessage() + " (" + srch.getErrorCode() + ")");
        }
        else if (!srch.getErrorMessage().equals("")) {
            System.out.println(srch.getErrorMessage() + " (" + srch.getErrorCode() + ")");
        }
        else {
            System.out.println(resultMapList);
        }

        // code to produce a sorted list (by attribute name) of results
        // useful when ReturningAttributesCsv (=null) is set to return every attribute
        /*
        Set<String> attrNameSet = new HashSet<>();
        for (Map<String, String> r : resultMapList) {
            attrNameSet.addAll(r.keySet());
        }
        List<String> attrNamesSorted = new ArrayList<>(attrNameSet);
        attrNamesSorted.sort(String.CASE_INSENSITIVE_ORDER);  // or Comparator.naturalOrder()
        for (String s : attrNamesSorted) {
            System.out.println(s);
        }
        for (Map<String, String> r : resultMapList) {
            System.out.println("==================================");
            for (String s : attrNamesSorted) {
                System.out.println(r.get(s));
            }
            System.out.println("==================================");
        }
        */

        start = System.currentTimeMillis();
        resultMapList = srch.doGetEntry("P4883103"); // serialNumber=P4883103 or uid=khb5 or uid=khboyes
        stop = System.currentTimeMillis();
        System.out.println("doGetEntry took " + (stop - start) + " ms");

        if (srch.getErrorCode() == DirectorySearchLDS.NO_MATCHES) {
            System.out.println(srch.getErrorMessage() + " (" + srch.getErrorCode() + ")");
        }
        else if (srch.getErrorCode() == DirectorySearchLDS.TOO_MANY) {
            System.out.println(srch.getErrorMessage() + " (" + srch.getErrorCode() + ")");
        }
        else if (!srch.getErrorMessage().equals("")) {
            System.out.println(srch.getErrorMessage() + " (" + srch.getErrorCode() + ")");
        }
        else {
            System.out.println(resultMapList);
        }
    }

    private static DirContext getDirContextRHEDcloud() {
        Hashtable<String, String> env = new Hashtable<>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, "ldap://ldap.vpc.rhedcloud.org/");
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PRINCIPAL, "cn=LDS Service,ou=Service Accounts,dc=rhedcloud,dc=org");
        env.put(Context.SECURITY_CREDENTIALS, RHEDCLOUD_SECURITY_CREDENTIALS);
        return getDirContext(env);
    }

    private static DirContext getDirContextEmory() {
        Hashtable<String, String> env = new Hashtable<>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, "ldaps://ldsauth.service.emory.edu");
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PRINCIPAL, "cn=ESBInternalView,OU=Services,DC=emory,DC=edu");
        env.put(Context.SECURITY_CREDENTIALS, EMORY_SECURITY_CREDENTIALS);
        return getDirContext(env);
    }

    private static DirContext getDirContextRice() {
        Hashtable<String, String> env = new Hashtable<>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, "ldaps://ldap-test-n1.iam.rice.edu");
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PRINCIPAL, "uid=rhedcloud,ou=Service Accounts,dc=rice,dc=edu");
        env.put(Context.SECURITY_CREDENTIALS, RICE_SECURITY_CREDENTIALS);
        return getDirContext(env);
    }

    private static DirContext getDirContext(Hashtable<String, String> env) {
        InitialDirContext ic;
        DirContext ctx;
        try {
            ic = new InitialDirContext(env);
        }
        catch (NamingException ne) {
            String errMsg = "An error occurred getting an initial context "
                    + "(connection to) the directory Server. The exception is: " + ne.getMessage();
            throw new RuntimeException(errMsg, ne);
        }

        try {
            ctx = (DirContext) ic.lookup("");
        }
        catch (NamingException ne) {
            String errMsg = "An error occurred getting a directory context "
                    + "(connection to the directory server) with which to perform the query. The exception is: "
                    + ne.getMessage();
            throw new RuntimeException(errMsg, ne);
        }

        return ctx;
    }
}
