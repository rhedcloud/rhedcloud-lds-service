package edu.emory.it.services.ldss.provider;

import edu.emory.moa.jmsobjects.identity.v2_0.FullPerson;
import edu.emory.moa.objects.resources.v2_0.FullPersonQuerySpecification;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.PropertyConfig;
import org.openeai.layouts.EnterpriseLayoutException;
import org.openeai.layouts.EnterpriseLayoutManager;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * An LDAP implementation of an FullPersonProvider.
 */
public class LdsFullPersonProvider extends OpenEaiObject implements FullPersonProvider {
    private static final String LOGTAG = "[LdsFullPersonProvider] ";
    private static final Logger logger = LogManager
            .getLogger("edu.emory.it.services.ldss");

    private AppConfig appConfig = null;
    private DirContext m_dirCtx = null;

    // Provider properties
    private String m_providerUrl = null;
    private String m_initialContextFactory = null;
    private String m_securityPrincipal = null;
    private String m_securityCredentials = null;
    private String m_userSearchBase = null;
    private String m_returningAttributes = null;
    private String m_sortKeys = null;
    private String m_basicSearchAttributes = null;
    private String m_primaryKey = null;
    private String m_searchRestriction = null;
    private boolean m_verbose = true;

    /**
     * This method initializes the provider making it ready to provide a
     * list of FullPerson to the calling application. It extracts
     * the Properties object from AppConfig, extracts needed properties,
     * and creates a Directory Context using properties, setting all of
     * them in private variables. The saved Dir Context is used to
     * generate a Dir Context for each transaction. It also initializes
     * the LDAP connection monitor, logging, and statistics capturing.
     *
     * @param aConfig AppConfig object containing everything this FullPersonProvider needs.
     * @throws ProviderException with details of the initialization error.
     */
    public void init(AppConfig aConfig) throws ProviderException {
        logger.info(LOGTAG + " - Initializing");
        appConfig = aConfig;

        // Get the provider properties from AppConfig
        Properties ldapProps;
        try {
            PropertyConfig pConfig = (PropertyConfig) appConfig.getObject("LdsFullPersonProvider");
            ldapProps = pConfig.getProperties();
            setProperties(ldapProps);
        }
        catch (EnterpriseConfigurationObjectException eoce) {
            String errMsg = "Error retrieving a PropertyConfig object from AppConfig: The exception is: " + eoce.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new ProviderException(errMsg);
        }

        /*
         * Set operational properties.
         */

        // Provider URL to connect to the directory
        String providerUrl = getProperties().getProperty("providerUrl");
        if (providerUrl == null || providerUrl.trim().equals("")) {
            String errMsg = "providerUrl property not specified. Can't continue.";
            logger.fatal(LOGTAG + errMsg);
            throw new ProviderException(errMsg);
        }
        else {
            setProviderUrl(providerUrl);
            logger.info(LOGTAG + "providerUrl is '" + getProviderUrl() + "'.");
        }

        /* Initial context factory to connect to the directory */
        String initialContextFactory = getProperties().getProperty("initialContextFactory");
        if (initialContextFactory == null || initialContextFactory.trim().equals("")) {
            String errMsg = "initialContextFactory property not specified. Can't continue.";
            logger.fatal(LOGTAG + errMsg);
            throw new ProviderException(errMsg);
        }
        else {
            setInitialContextFactory(initialContextFactory);
            logger.info(LOGTAG + "initialContextFactory is '" + getInitialContextFactory() + "'.");
        }

        /* Security principal (service account DN) with which to connect to the directory. */
        String securityPrincipal = getProperties().getProperty("securityPrincipal");
        if (securityPrincipal == null || securityPrincipal.trim().equals("")) {
            String errMsg = "securityPrincipal property not specified. Can't continue.";
            logger.fatal(LOGTAG + errMsg);
            throw new ProviderException(errMsg);
        }
        else {
            setSecurityPrincipal(securityPrincipal);
            logger.info(LOGTAG + "securityPrincipal is '" + getSecurityPrincipal() + "'.");
        }

        /* Security credentials (password) with which to connect to the directory. */
        String securityCredentials = getProperties().getProperty("securityCredentials");
        if (securityCredentials == null || securityCredentials.equals("")) {
            String errMsg = "securityCredentials property not specified. Can't continue.";
            logger.fatal(LOGTAG + errMsg);
            throw new ProviderException(errMsg);
        }
        else {
            setSecurityCredentials(securityCredentials);
            logger.info(LOGTAG + "securityCredentials are present.");
        }

        // Verbose logging.
        boolean verbose = Boolean.parseBoolean(getProperties().getProperty("verbose", "false"));
        setVerbose(verbose);
        if (getVerbose()) {
            logger.info(LOGTAG + "Verbose logging enabled.");
        } else {
            logger.info(LOGTAG + "Verbose logging disabled.");
        }

        /* User search base. */
        String userSearchBase = getProperties().getProperty("userSearchBase");
        if (userSearchBase == null || userSearchBase.trim().equals("")) {
            String errMsg = "userSearchBase property not specified. Can't continue.";
            logger.fatal(LOGTAG + errMsg);
            throw new ProviderException(errMsg);
        }
        else {
            setUserSearchBase(userSearchBase);
            logger.info(LOGTAG + "userSearchBase is '" + getUserSearchBase() + "'.");
        }

        String returningAttributes = getProperties().getProperty("returningAttributes");
        if (returningAttributes == null || returningAttributes.trim().equals("")) {
            String errMsg = "returningAttributes property not specified. Can't continue.";
            logger.fatal(LOGTAG + errMsg);
            throw new ProviderException(errMsg);
        }
        else {
            setReturningAttributes(returningAttributes.trim().replaceAll("[\n\t\r ]", ""));
            logger.info(LOGTAG + "returningAttributes is '" + getReturningAttributes() + "'.");
        }

        String sortKeys = getProperties().getProperty("sortKeys");
        if (sortKeys == null || sortKeys.trim().equals("")) {
            String errMsg = "sortKeys property not specified. Can't continue.";
            logger.fatal(LOGTAG + errMsg);
            throw new ProviderException(errMsg);
        }
        else {
            setSortKeys(sortKeys.trim().replaceAll("[\n\t\r ]", ""));
            logger.info(LOGTAG + "sortKeys is '" + getSortKeys() + "'.");
        }

        String basicSearchAttributes = getProperties().getProperty("basicSearchAttributes");
        if (basicSearchAttributes == null || basicSearchAttributes.trim().equals("")) {
            String errMsg = "basicSearchAttributes property not specified. Can't continue.";
            logger.fatal(LOGTAG + errMsg);
            throw new ProviderException(errMsg);
        }
        else {
            setBasicSearchAttributes(basicSearchAttributes.trim().replaceAll("[\n\t\r ]", ""));
            logger.info(LOGTAG + "basicSearchAttributes is '" + getBasicSearchAttributes() + "'.");
        }

        String searchRestriction = getProperties().getProperty("searchRestriction");
        if (searchRestriction == null || searchRestriction.trim().equals("")) {
            logger.info(LOGTAG + "searchRestriction is not specified.");
        }
        else {
            setSearchRestriction(searchRestriction);
            logger.info(LOGTAG + "searchRestriction is '" + getSearchRestriction() + "'.");
        }

        String primaryKey = getProperties().getProperty("primaryKey");
        if (primaryKey == null || primaryKey.trim().equals("")) {
            String errMsg = "primaryKey property not specified. Can't continue.";
            logger.fatal(LOGTAG + errMsg);
            throw new ProviderException(errMsg);
        }
        else {
            setPrimaryKey(primaryKey);
            logger.info(LOGTAG + "primaryKey is '" + getPrimaryKey() + "'.");
        }

        /* Initialize a directory context to query the directory. */
        try {
            logger.info(LOGTAG + "Connecting to the directory server...");
            m_dirCtx = getInitialContext(getProperties());
            logger.info(LOGTAG + "Connected to the directory server.");
        }
        catch (NamingException ne) {
            String errMsg = "An error occurred getting an initial context "
                    + "(connection to) the directory Server. The exception is: " + ne.getMessage();
            throw new ProviderException(errMsg, ne);
        }

        logger.info(LOGTAG + "Initialized.");
    }

    /**
     * This method provides a List of FullPerson by calling a provider to look up the people.
     *
     * @param qs FullPersonQuerySpecification, the query object to use to retrieve a List of FullPerson.
     * @throws ProviderException on error
     * @throws DirectorySearchException with details of the error in retrieving the people.
     */
    public List<FullPerson> query(FullPersonQuerySpecification qs) throws ProviderException, DirectorySearchException {
        String mLOGTAG = "[LdsFullPersonProvider.query] ";
        DirContext ctx;
        try {
            ctx = getDirContext();
        }
        catch (NamingException ne) {
            String errMsg = "An error occurred getting a directory context "
                    + "(connection to the directory server) with which to perform the query. The exception is: "
                    + ne.getMessage();
            throw new ProviderException(errMsg, ne);
        }

        /*
         * Here call out to the directory search routine. Always search by key (PublicId).
         * The result is an array list of hash maps that each map the names of the
         * LDAP attributes to their values.
         */
        DirectorySearchLDS srch = new DirectorySearchLDS(ctx);
        srch.setLdapSearchBase(getUserSearchBase());
        srch.setReturningAttributes(getReturningAttributes());
        srch.setPrimaryKey(getPrimaryKey());
        srch.setSortKeys(getSortKeys());
        srch.setBasicSearchAttributes(getBasicSearchAttributes());
        srch.setSearchRestriction(getSearchRestriction());

        String publicId = qs.getPublicId();

        List<Map<String, String>> resultMapList;
        long queryStart = System.currentTimeMillis();
        String queryType;

        if (publicId == null || publicId.trim().equals("")) {
            throw new DirectorySearchException("FullPerson query only supports PublicId");
        }
        else {
            publicId = publicId.trim();
            logger.info(mLOGTAG + "PublicId specified, searching for: " + publicId);
            queryType = "GetEntry";
            resultMapList = srch.doGetEntry(publicId);
        }

        long queryElapsed = System.currentTimeMillis() - queryStart;

        /*
         * Return any message from DirectorySearch. Must use an exception to get the
         * message sent all the way up to FullPersonRequestCommand, which
         * catches exceptions and puts appropriate message in the reply and puts the
         * message in the log. We use a separate exception so we can format the
         * message with the message code and the handler can log as fatal only
         * actual exceptions thrown by DirectorySearch. That keeps bogus fatal
         * errors out of the logs. It is understood that if DirectorySearch returns
         * an error, then it returns no entries, so OK to throw an error and abort.
         */
        String srchErrMsg = srch.getErrorMessage();
        if (!srchErrMsg.equals("")) {
            throw new DirectorySearchException(srch.getErrorCode() + " " + srchErrMsg);
        }

        /*
         * One or more entries were returned, so put the data into an list of
         * FullPerson. This code defines the correspondence between the
         * attributes of the LDAP directory and the field names of the
         * FullPerson.
         */
        List<FullPerson> queryResults = new ArrayList<>();
        logger.info(mLOGTAG + "searched using " + queryType + " took " + queryElapsed + " millis" +
                " with the number of rows in response list = " + resultMapList.size());

        EnterpriseLayoutManager elmOther = null;
        EnterpriseLayoutManager elmXml = null;

        for (Map<String, String> pMap : resultMapList) {
            try {
                logger.info(mLOGTAG + "result map = " + pMap.toString());

                FullPerson myfp = (FullPerson) appConfig.getObjectByType(FullPerson.class.getName());
                queryResults.add(myfp);

                // only need to get them once
                if (elmOther == null) {
                    elmOther = (EnterpriseLayoutManager) myfp.getInputLayoutManagers().get("other");
                }
                if (elmXml == null) {
                    elmXml = (EnterpriseLayoutManager) myfp.getInputLayoutManagers().get("xml");
                }

                myfp.setInputLayoutManager(elmOther);
                myfp.buildObjectFromInput(pMap);
                myfp.setInputLayoutManager(elmXml);
            }
            catch (EnterpriseConfigurationObjectException e) {
                String errMsg = "Error retrieving 'FullPerson' from AppConfig: The exception is: " + e.getMessage();
                logger.fatal(mLOGTAG + errMsg);
                throw new ProviderException(errMsg, e);
            }
            catch (EnterpriseLayoutException e) {
                String errMsg = "Error parsing attributes to a 'FullPerson': The exception is: " + e.getMessage();
                logger.fatal(mLOGTAG + errMsg);
                throw new ProviderException(errMsg, e);
            }
        }

        return queryResults;
    }

    /** This method sets the value of the provider URL. @param providerUrl String */
    private void setProviderUrl(String providerUrl) { m_providerUrl = providerUrl; }
    /** This method returns the value of the provider URL. @return String, the provider URL. */
    private String getProviderUrl() { return m_providerUrl; }
    /** This method sets the value of the security principal. @param securityPrincipal String */
    private void setSecurityPrincipal(String securityPrincipal) { m_securityPrincipal = securityPrincipal; }
    /** This method returns the value of the security principal. @return String, security principal. */
    private String getSecurityPrincipal() { return m_securityPrincipal; }
    /** This method sets the value of the security credentials. @param securityCredentials String */
    private void setSecurityCredentials(String securityCredentials) { m_securityCredentials = securityCredentials; }
    /** This method returns the value of the security credentials. @return String, security credentials. */
    private String getSecurityCredentials() { return m_securityCredentials; }
    /** This method sets the value of the initial context factory. @param initialContextFactory String */
    private void setInitialContextFactory(String initialContextFactory) { m_initialContextFactory = initialContextFactory; }
    /** This method returns the value of the initial context factory. @return String, initial context factory. */
    private String getInitialContextFactory() { return m_initialContextFactory; }
    /** Get the value of userSearchBase @return String, the value of userSearchBase */
    public String getUserSearchBase() { return m_userSearchBase; }
    /** Set the value of userSearchBase @param userSearchBase String, new value of userSearchBase */
    public void setUserSearchBase(String userSearchBase) { m_userSearchBase = userSearchBase; }
    /** Get the value of returningAttributes @return String, the value of returningAttributes */
    public String getReturningAttributes() { return m_returningAttributes; }
    /** Set the value of returningAttributes @param returningAttributes String, new value of returningAttributes */
    public void setReturningAttributes(String returningAttributes) { m_returningAttributes = returningAttributes; }
    /** Get the value of sortKeys @return String, the value of sortKeys */
    public String getSortKeys() { return m_sortKeys; }
    /** Set the value of sortKeys @param sortKeys String, new value of sortKeys */
    public void setSortKeys(String sortKeys) { m_sortKeys = sortKeys; }
    /** Get the value of basicSearchAttributes @return String, the value of basicSearchAttributes */
    public String getBasicSearchAttributes() { return m_basicSearchAttributes; }
    /** Set the value of basicSearchAttributes @param basicSearchAttributes String, new value of basicSearchAttributes */
    public void setBasicSearchAttributes(String basicSearchAttributes) { m_basicSearchAttributes = basicSearchAttributes; }
    /** Get the value of searchRestriction @return String, the value of searchRestriction */
    public String getSearchRestriction() { return m_searchRestriction; }
    /** Set the value of searchRestriction @param searchRestriction String, new value of searchRestriction */
    public void setSearchRestriction(String searchRestriction) { m_searchRestriction = searchRestriction; }
    /** Get the value of primaryKey @return String, the value of primaryKey */
    public String getPrimaryKey() { return m_primaryKey; }
    /** Set the value of primaryKey @param primaryKey String, new value of primaryKey */
    public void setPrimaryKey(String primaryKey) { m_primaryKey = primaryKey; }
    /** This method sets the value of the verbose parameter @param b boolean */
    private void setVerbose(boolean b) { m_verbose = b; }
    /** This method returns the boolean value of the verbose parameter. @return boolean, verbose logging. */
    private boolean getVerbose() { return m_verbose; }

    /**
     * Gets an initial dir context to use for accessing the directory
     *
     * @param props Properties object from which to get environment values to use
     * @return initial dir context
     * @throws NamingException on error
     */
    private synchronized DirContext getInitialContext(Properties props) throws NamingException {
        String mLOGTAG = "[LdsFullPersonProvider.getInitialContext] ";

        // Initialize a directory context and environment hash table with which
        // we will create the initial context object.
        InitialDirContext ic;
        Hashtable<String, String> env = new Hashtable<>();

        /*
         * Set context environment values.
         * There is no need to set the Context.SECURITY_PROTOCOL. This is handled by
         * the initialContextFactory. Also the providerURL should not be changed, so
         * that it can be reused elsewhere (not currently reused).
         */
        env.put(Context.INITIAL_CONTEXT_FACTORY, getInitialContextFactory());
        env.put(Context.PROVIDER_URL, getProviderUrl());
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PRINCIPAL, getSecurityPrincipal());
        env.put(Context.SECURITY_CREDENTIALS, getSecurityCredentials());

        logger.info(mLOGTAG + "Creating a new initial context object (establishing a connection to the LDAP server)...");
        ic = new InitialDirContext(env);
        logger.info(mLOGTAG + "Created the InitialContext object.");

        return ic;
    }

    /*
     * This method gets a directory context to use to query the directory server.
     * First, the method tries to create a new context by performing a blank
     * lookup on the existing context that was created by init. Directory
     * connections can time out and get reset, so if that does not work it
     * attempts to create a new initial context.
     *
     * @return DirContext, a directory context (connection to the directory server).
     */
    private DirContext getDirContext() throws NamingException {
        String mLOGTAG = "[LdsFullPersonProvider.getDirContext] ";
        logger.debug(mLOGTAG + "Getting a directory context to use in a transaction.");
        DirContext dirCtx;

        try {
            dirCtx = (DirContext) m_dirCtx.lookup("");
            logger.info(mLOGTAG + "Successfully created a context with a lookup on an existing context.");
        }
        catch (NamingException ne1) {
            // An error occurred retrieving a directory context. Log it, attempt
            // to create a new initial context, and then try to get another context to return for use.
            String errMsg = "An error occurred retrieving a usable directory context. The exception is: " + ne1.getMessage() + ".";
            logger.warn(mLOGTAG + errMsg);
            try {
                logger.info(mLOGTAG + "Attempting to create a new initial context to the directory server...");
                m_dirCtx = getInitialContext(getProperties());
                logger.info(mLOGTAG + "Created a new initial context.");
            }
            catch (NamingException ne2) {
                // An error occurred re-creating a new initial context. Log it
                // and throw an exception.
                errMsg = "An error occurred re-creating a new initial context. The exception is: " + ne2.getMessage();
                logger.fatal(mLOGTAG + errMsg);
                throw new NamingException(errMsg);
            }
            try {
                logger.info(mLOGTAG + "Attempting to create a new context from a lookup on the new initial context.");
                dirCtx = (DirContext) m_dirCtx.lookup("");
                logger.info(mLOGTAG + "Successfully created a new context.");
            }
            catch (NamingException ne3) {
                // An error occurred retrieving a usable directory context using
                // the re-created directory context. Log it and throw an
                // exception.
                errMsg = "An error occurred retrieving a usable directory context "
                        + "using the re-created directory context. The exception is: " + ne3.getMessage();
                logger.fatal(mLOGTAG + errMsg);
                throw new NamingException(errMsg);
            }
        }
        return dirCtx;
    }
}
