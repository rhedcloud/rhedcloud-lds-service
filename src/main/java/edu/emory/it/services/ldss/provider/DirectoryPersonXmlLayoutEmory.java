package edu.emory.it.services.ldss.provider;

import edu.emory.moa.jmsobjects.identity.v1_0.DirectoryPerson;
import edu.emory.moa.objects.resources.v1_0.Email;
import org.jdom.Document;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.layouts.EnterpriseLayoutException;
import org.openeai.layouts.EnterpriseLayoutManager;
import org.openeai.layouts.EnterpriseLayoutManagerImpl;
import org.openeai.moa.XmlEnterpriseObject;

import java.io.Serializable;
import java.util.Map;

public class DirectoryPersonXmlLayoutEmory extends EnterpriseLayoutManagerImpl implements EnterpriseLayoutManager, Serializable {
    public void init(String layoutManagerName, Document layoutDoc) throws EnterpriseLayoutException {
        super.init(layoutManagerName, layoutDoc);
    }

    @Override
    public void buildObjectFromInput(Object input, XmlEnterpriseObject xeo) throws EnterpriseLayoutException {
        @SuppressWarnings("unchecked")
        Map<String, String> pMap = (Map<String, String>) input;
        DirectoryPerson mydp = (DirectoryPerson) xeo;

        try {
            // required fields
            mydp.setFullName(pMap.get("fullName"));
            mydp.setKey(pMap.get("serialNumber"));
            if (pMap.containsKey("organizationalStatus") && !pMap.get("organizationalStatus").equals("")) {
                mydp.setType(pMap.get("organizationalStatus"));
            } else {
                mydp.setType("Healthcare");
            }

            // optional fields
            if (pMap.containsKey("emoryGivenName"))
                mydp.setFirstMiddle(pMap.get("emoryGivenName"));
            if (pMap.containsKey("sn"))
                mydp.setLastName(pMap.get("sn"));
            if (pMap.containsKey("mail") && !pMap.get("mail").equals("")) {
                Email em = mydp.newEmail();
                em.setEmailAddress(pMap.get("mail"));
                em.setType("Primary");
                mydp.setEmail(em);
            }
            if (pMap.containsKey("postalAddress"))
                mydp.setMailStop(pMap.get("postalAddress"));
            if (pMap.containsKey("telephoneNumber"))
                mydp.setDirectoryPhone(pMap.get("telephoneNumber"));
            if (pMap.containsKey("facsimileTelephoneNumber"))
                mydp.setFax(pMap.get("facsimileTelephoneNumber"));
            if (pMap.containsKey("ou"))
                mydp.setDepartmentName(pMap.get("ou"));
            if (pMap.containsKey("businessCategory"))
                mydp.setSchoolDivision(pMap.get("businessCategory"));
            if (pMap.containsKey("title"))
                mydp.setTitle(pMap.get("title"));
            if (pMap.containsKey("emorystudentphone"))
                mydp.setStudentPhone(pMap.get("emorystudentphone"));
            if (pMap.containsKey("physicalDeliveryOfficeName"))
                mydp.setDirectoryLocation(pMap.get("physicalDeliveryOfficeName"));
            if (pMap.containsKey("generationQualifier"))
                mydp.setSuffix(pMap.get("generationQualifier"));
        }
        catch (EnterpriseFieldException e) {
            String errMsg = "Error filling DirectoryPerson object from map. The exception is: " + e.getMessage();
            throw new EnterpriseLayoutException(errMsg, e);
        }
    }

    @Override
    public Object buildOutputFromObject(XmlEnterpriseObject xeo) throws EnterpriseLayoutException {
        return null;
    }

    @Override
    public Object buildOutputFromObject(XmlEnterpriseObject xeo, String appName) throws EnterpriseLayoutException {
        setTargetAppName(appName);
        return buildOutputFromObject(xeo);
    }
}
