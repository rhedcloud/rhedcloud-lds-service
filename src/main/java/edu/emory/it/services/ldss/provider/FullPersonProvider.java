package edu.emory.it.services.ldss.provider;

import edu.emory.moa.jmsobjects.identity.v2_0.FullPerson;
import edu.emory.moa.objects.resources.v2_0.FullPersonQuerySpecification;
import org.openeai.config.AppConfig;

import java.util.List;

/**
 * Interface for all Full Person providers.
 */
public interface FullPersonProvider {
    /**
     * @param aConfig an AppConfig object with all this provider needs
     * @throws ProviderException with details of the initialization error
     */
    void init(AppConfig aConfig) throws ProviderException;

    /**
     * @param aqs FullPersonQuerySpecification, the query parameters
     * @return List of FullPerson
     * @throws DirectorySearchException directory search errors
     * @throws ProviderException with details of the error
     */
    List<FullPerson> query(FullPersonQuerySpecification aqs) throws ProviderException, DirectorySearchException;
}