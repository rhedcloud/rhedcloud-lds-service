package edu.emory.it.services.ldss.provider;

import edu.emory.moa.jmsobjects.lightweightdirectoryservices.v1_0.Group;
import edu.emory.moa.jmsobjects.lightweightdirectoryservices.v1_0.OrganizationalUnit;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.directory.api.ldap.model.entry.Attribute;
import org.apache.directory.api.ldap.model.entry.DefaultModification;
import org.apache.directory.api.ldap.model.entry.Entry;
import org.apache.directory.api.ldap.model.entry.Modification;
import org.apache.directory.api.ldap.model.entry.ModificationOperation;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.api.ldap.model.exception.LdapInvalidAttributeValueException;
import org.apache.directory.api.util.Strings;
import org.apache.directory.ldap.client.api.DefaultLdapConnectionFactory;
import org.apache.directory.ldap.client.api.LdapConnection;
import org.apache.directory.ldap.client.api.LdapConnectionConfig;
import org.apache.directory.ldap.client.api.LdapConnectionPool;
import org.apache.directory.ldap.client.api.ValidatingPoolableLdapConnectionFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public final class LdsProviderUtil {
    private static final Logger logger = LogManager
            .getLogger("edu.emory.it.services.ldss");
    private static final char[] HEX_DIGITS = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    private static LdapConnectionPool ldapConnectionPool;

    public static final String[] returningAttributes = {
            "distinguishedName",
            "objectClass",
            "structuralObjectClass",
            "ou",
            "cn",
            "name",
            "description",
            "groupType",
            "objectSid",
            "member",
            "objectGUID",
            "instanceType",
            "objectCategory",
            "dSCorePropagationData",
            "subSchemaSubEntry",
            "uSNCreated",
            "uSNChanged",
            "whenCreated",
            "whenChanged",
            "createTimeStamp",
            "modifyTimeStamp"};

    public static void setProviderProperties(OpenEaiObject eaiObject, AppConfig appConfig, String propName) throws ProviderException {
        try {
            eaiObject.setProperties(appConfig.getProperties(propName));
        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "Error retrieving the " + propName
                    + " PropertyConfig object from AppConfig: The exception is: " + e.getMessage();
            logger.error(errMsg, e);
            throw new ProviderException(errMsg, e);
        }
    }

    /**
     * Populate an OrganizationalUnit from an Entry returned from the LDAP query
     *
     * @param ou to populate
     * @param ent LDAP entry
     * @return populated OrganizationalUnit
     * @throws ProviderException on error
     * @throws LdapInvalidAttributeValueException on error
     * @throws EnterpriseFieldException on error
     */
    public static OrganizationalUnit populateOrganizationalUnit(OrganizationalUnit ou, Entry ent)
            throws ProviderException, LdapInvalidAttributeValueException, EnterpriseFieldException {

        if (!ent.hasObjectClass("organizationalUnit")) {
            throw new ProviderException("Entry is not an OrganizationalUnit");
        }

        for (Attribute att : ent.getAttributes()) {
            switch (att.getUpId()) {
                case "distinguishedName":
                    // required=true
                    ou.setdistinguishedName(att.getString());
                    break;
                case "objectClass":
                    // required=true
                    att.forEach(v -> ou.addobjectClass(v.getString()));
                    break;
                case "structuralObjectClass": // OID 2.5.21.9
                    att.forEach(v -> ou.addstructuralObjectClass(v.getString()));
                    break;
                case "ou":
                    ou.setou(att.getString());
                    break;
                case "name":
                    ou.setname(att.getString());
                    break;
                case "description":
                    ou.setdescription(att.getString());
                    break;
                case "objectGUID":
                    ou.setobjectGUID(convertObjectGuidToString(att.getBytes()));
                    break;
                case "instanceType":
                    ou.setinstanceType(att.getString());
                    break;
                case "objectCategory":
                    ou.setobjectCategory(att.getString());
                    break;
                case "dSCorePropagationData":
                    ou.setdsCorePropagationData(att.getString());
                    break;
                case "subSchemaSubEntry":  // OID 2.5.18.10
                    att.forEach(v -> ou.addsubSchemaSubEntry(v.getString()));
                    break;
                case "uSNCreated":
                    ou.setuSNCreated(att.getString());
                    break;
                case "uSNChanged":
                    ou.setuSNChanged(att.getString());
                    break;
                case "whenCreated":
                    ou.setwhenCreated(att.getString());
                    break;
                case "whenChanged":
                    ou.setwhenChanged(att.getString());
                    break;
                case "createTimeStamp": // OID 2.5.18.1
                    ou.setcreateTimeStamp(att.getString());
                    break;
                case "modifyTimeStamp": // OID 2.5.18.2
                    ou.setmodifyTimeStamp(att.getString());
                    break;
                default:
                    throw new ProviderException("Unknown attribute: " + att.getUpId());
            }
        }

        return ou;
    }

    /**
     * Populate a Group from an Entry returned from the LDAP query
     *
     * @param group to populate
     * @param ent LDAP entry
     * @return populated OrganizationalUnit
     * @throws ProviderException on error
     * @throws LdapInvalidAttributeValueException on error
     * @throws EnterpriseFieldException on error
     */
    public static Group populateGroup(Group group, Entry ent)
            throws ProviderException, LdapInvalidAttributeValueException, EnterpriseFieldException {

        if (!ent.hasObjectClass("group")) {
            throw new ProviderException("Entry is not an Group");
        }

        for (Attribute att : ent.getAttributes()) {
            switch (att.getUpId()) {
                case "distinguishedName":
                    // required=true
                    group.setdistinguishedName(att.getString());
                    break;
                case "objectClass":
                    // required=true
                    att.forEach(v -> group.addobjectClass(v.getString()));
                    break;
                case "structuralObjectClass": // OID 2.5.21.9
                    att.forEach(v -> group.addstructuralObjectClass(v.getString()));
                    break;
                case "cn":
                    group.setcn(att.getString());
                    break;
                case "name":
                    group.setname(att.getString());
                    break;
                case "description":
                    group.setdescription(att.getString());
                    break;
                case "groupType":
                    group.setgroupType(att.getString());
                    break;
                case "objectSid":
                    group.setobjectSid(convertObjectSidToString(att.getBytes()));
                    break;
                case "member":
                    att.forEach(v -> group.addmember(v.getString()));
                    break;
                case "objectGUID":
                    group.setobjectGUID(convertObjectGuidToString(att.getBytes()));
                    break;
                case "instanceType":
                    group.setinstanceType(att.getString());
                    break;
                case "objectCategory":
                    group.setobjectCategory(att.getString());
                    break;
                case "dSCorePropagationData":
                    group.setdsCorePropagationData(att.getString());
                    break;
                case "subSchemaSubEntry":  // OID 2.5.18.10
                    att.forEach(v -> group.addsubSchemaSubEntry(v.getString()));
                    break;
                case "uSNCreated":
                    group.setuSNCreated(att.getString());
                    break;
                case "uSNChanged":
                    group.setuSNChanged(att.getString());
                    break;
                case "whenCreated":
                    group.setwhenCreated(att.getString());
                    break;
                case "whenChanged":
                    group.setwhenChanged(att.getString());
                    break;
                case "createTimeStamp": // OID 2.5.18.1
                    group.setcreateTimeStamp(att.getString());
                    break;
                case "modifyTimeStamp": // OID 2.5.18.2
                    group.setmodifyTimeStamp(att.getString());
                    break;
                default:
                    throw new ProviderException("Unknown attribute: " + att.getUpId());
            }
        }

        return group;
    }

    // taken from InPlaceMsAdObjectGuidValueEditor in Apache Directory Studio
    private static String convertObjectGuidToString(byte[] bytes) {
        char[] hex = encodeHex(bytes);
        StringBuilder sb = new StringBuilder();
        sb.append('{');
        sb.append(hex, 6, 2);
        sb.append(hex, 4, 2);
        sb.append(hex, 2, 2);
        sb.append(hex, 0, 2);
        sb.append('-');
        sb.append(hex, 10, 2);
        sb.append(hex, 8, 2);
        sb.append('-');
        sb.append(hex, 14, 2);
        sb.append(hex, 12, 2);
        sb.append('-');
        sb.append(hex, 16, 4);
        sb.append('-');
        sb.append(hex, 20, 12);
        sb.append('}');

        return Strings.toLowerCaseAscii(sb.toString());
    }

    // taken from InPlaceMsAdObjectSidValueEditor in Apache Directory Studio
    private static String convertObjectSidToString(byte[] bytes) {
        /*
         * The binary data structure, from http://msdn.microsoft.com/en-us/library/cc230371(PROT.10).aspx:
         *   byte[0] - Revision (1 byte): An 8-bit unsigned integer that specifies the revision level of the SID structure. This value MUST be set to 0x01.
         *   byte[1] - SubAuthorityCount (1 byte): An 8-bit unsigned integer that specifies the number of elements in the SubAuthority array. The maximum number of elements allowed is 15.
         *   byte[2-7] - IdentifierAuthority (6 bytes): A SID_IDENTIFIER_AUTHORITY structure that contains information, which indicates the authority under which the SID was created. It describes the entity that created the SID and manages the account.
         *               Six element arrays of 8-bit unsigned integers that specify the top-level authority
         *               big-endian!
         *   and then - SubAuthority (variable): A variable length array of unsigned 32-bit integers that uniquely identifies a principal relative to the IdentifierAuthority. Its length is determined by SubAuthorityCount.
         *              little-endian!
         */

        char[] hex = encodeHex(bytes);
        StringBuilder sb = new StringBuilder();

        // start with 'S'
        sb.append('S');

        // revision
        int revision = Integer.parseInt(new String(hex, 0, 2), 16);
        sb.append('-');
        sb.append(revision);

        // get count
        int count = Integer.parseInt(new String(hex, 2, 2), 16);

        // check length
        if (bytes.length != (8 + count * 4)) {
            // the SID has a format that isn't expected so dump the hex
            return Strings.dumpHexPairs(bytes);
        }

        // get authority, big-endian
        long authority = Long.parseLong(new String(hex, 4, 12), 16);
        sb.append('-');
        sb.append(authority);

        // sub-authorities, little-endian
        for (int i = 0; i < count; i++) {
            StringBuilder rid = new StringBuilder();

            for (int k = 3; k >= 0; k--) {
                rid.append(hex[16 + (i * 8) + (k * 2)]);
                rid.append(hex[16 + (i * 8) + (k * 2) + 1]);
            }

            long subAuthority = Long.parseLong(rid.toString(), 16);
            sb.append('-');
            sb.append(subAuthority);
        }

        return sb.toString();
    }

    // taken from Apache Directory Studio
    private static char[] encodeHex(byte[] data) {
        int l = data.length;
        char[] out = new char[l << 1];
        int i = 0;

        for(int var5 = 0; i < l; ++i) {
            out[var5++] = HEX_DIGITS[(240 & data[i]) >>> 4];
            out[var5++] = HEX_DIGITS[15 & data[i]];
        }

        return out;
    }

    public static void determineMod(String attributeName, String attribute, String baselineAttribute, List<Modification> mods) {
        if (baselineAttribute == null) {
            if (attribute != null) {
                mods.add(new DefaultModification(ModificationOperation.ADD_ATTRIBUTE, attributeName, attribute));
            }
            // else no modification
        } else {
            if (attribute == null) {
                mods.add(new DefaultModification(ModificationOperation.REMOVE_ATTRIBUTE, attributeName));
            } else if (!baselineAttribute.equals(attribute)) {
                mods.add(new DefaultModification(ModificationOperation.REPLACE_ATTRIBUTE, attributeName, attribute));
            }
            // else no modification
        }
    }

    public static void determineMod(boolean isGroup, String attributeName, List<String> attribute, List<String> baselineAttribute, List<Modification> mods) {
        if ("objectClass".equals(attributeName)) {
            Set<String> mandatory = new HashSet<>();
            Predicate<String> notMandatory = v -> !mandatory.contains(v);
            if (isGroup) {
                mandatory.add("group");
                mandatory.add("top");
            } else {
                mandatory.add("organizationalUnit");
                mandatory.add("top");
            }

            Set<String> filteredAttribute = attribute.stream().filter(notMandatory).collect(Collectors.toSet());
            Set<String> filteredBaselineAttribute = baselineAttribute.stream().filter(notMandatory).collect(Collectors.toSet());

            if (!filteredBaselineAttribute.equals(filteredAttribute)) {
                filteredAttribute.addAll(mandatory);
                mods.add(new DefaultModification(ModificationOperation.REPLACE_ATTRIBUTE, attributeName,
                        filteredAttribute.toArray(new String[filteredAttribute.size()])));
            }
        }
    }

    public static LdapConnection getLdapConnection() throws LdapException {
        return ldapConnectionPool.getConnection();
    }

    public static void releaseLdapConnection(LdapConnection connection) {
        try {
            if (connection != null)
                ldapConnectionPool.releaseConnection(connection);
        } catch (LdapException e) {
            logger.error("Error releasing LDAP connection back to pool", e);
        }
    }

    public static synchronized void closeLdapPool() {
        if (ldapConnectionPool != null) {
            try {
                ldapConnectionPool.close();
            } catch (Exception e) {
                logger.error("Error while closing LDAP connection pool", e);
            }
            ldapConnectionPool = null;
        }
    }
    public static synchronized void initLdapPool(String ldapHost, int ldapPort, String adminDn, String adminPassword) {
        if (ldapConnectionPool != null) {
            return; // already initialized
        }

        LdapConnectionConfig config = new LdapConnectionConfig();
        config.setLdapHost(ldapHost);
        config.setLdapPort(ldapPort);
        config.setName(adminDn);
        config.setCredentials(adminPassword);
        //config.setTrustManagers(new NoVerificationTrustManager());
        config.setUseTls(true);

        DefaultLdapConnectionFactory factory = new DefaultLdapConnectionFactory(config);
        factory.setTimeOut(30000L); // default

        // optional, values below are defaults
        GenericObjectPool.Config poolConfig = new GenericObjectPool.Config();
        poolConfig.lifo = true;
        poolConfig.maxActive = 8;
        poolConfig.maxIdle = 8;
        poolConfig.maxWait = -1L;
        poolConfig.minEvictableIdleTimeMillis = 1000L * 60L * 30L;
        poolConfig.minIdle = 0;
        poolConfig.numTestsPerEvictionRun = 3;
        poolConfig.softMinEvictableIdleTimeMillis = -1L;
        poolConfig.testOnBorrow = false;
        poolConfig.testOnReturn = false;
        poolConfig.testWhileIdle = false;
        poolConfig.timeBetweenEvictionRunsMillis = -1L;
        poolConfig.whenExhaustedAction = GenericObjectPool.WHEN_EXHAUSTED_BLOCK;

        ldapConnectionPool = new LdapConnectionPool(new ValidatingPoolableLdapConnectionFactory(factory), poolConfig);
    }
}
