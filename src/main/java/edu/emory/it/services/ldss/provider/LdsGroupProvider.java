package edu.emory.it.services.ldss.provider;

import edu.emory.moa.jmsobjects.lightweightdirectoryservices.v1_0.Group;
import edu.emory.moa.objects.resources.v1_0.GroupQuerySpecification;
import org.apache.directory.api.ldap.model.cursor.EntryCursor;
import org.apache.directory.api.ldap.model.entry.DefaultEntry;
import org.apache.directory.api.ldap.model.entry.Entry;
import org.apache.directory.api.ldap.model.entry.Modification;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.api.ldap.model.message.SearchScope;
import org.apache.directory.ldap.client.api.LdapConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;

import java.util.ArrayList;
import java.util.List;

public class LdsGroupProvider extends OpenEaiObject implements GroupProvider {
    private static final String LOGTAG = "[LdsGroupProvider]";
    private static final Logger logger = LogManager
            .getLogger("edu.emory.it.services.ldss");

    private static final String PROVIDER_PROPERTIES = "ldsGroupProviderProperties";

    // PropertyConfig keys for this provider
    private static final String PROP_LDAP_HOST = "ldapHost";
    private static final String PROP_LDAP_PORT = "ldapPort";
    private static final String PROP_LDAP_ADMIN_DN = "adminDn";
    private static final String PROP_LDAP_ADMIN_PASSWORD = "adminPassword";

    /*
     * default values, if applicable, for properties are specified here,
     * but can be overridden at runtime via corresponding properties from config doc
     */
    /** LDAP host - PROP_LDAP_HOST - no default */
    private String ldapHost;
    /** LDAP port - PROP_LDAP_PORT */
    private int ldapPort = 389;
    /** LDAP admin DN - PROP_LDAP_ADMIN_DN - no default */
    private String adminDn;
    /** LDAP admin password - PROP_LDAP_ADMIN_PASSWORD - no default */
    private String adminPassword;

    private AppConfig appConfig;


    @Override
    public void init(AppConfig aConfig) throws ProviderException {
        logger.info(LOGTAG + " - Initializing");
        appConfig = aConfig;
        refreshLocalVariables();
    }

    @Override
    public List<Group> query(GroupQuerySpecification qs) throws ProviderException {
        performPreActionSetup();

        long startTiming = System.currentTimeMillis();

        LdapConnection connection = null;
        try {
            connection = LdsProviderUtil.getLdapConnection();

            List<Group> results = new ArrayList<>();

            String dn = qs.getdistinguishedName();
            EntryCursor cursor = connection.search(dn, "(objectClass=group)", SearchScope.OBJECT,
                    LdsProviderUtil.returningAttributes);

            for (Entry ent : cursor) {
                Group group = (Group) appConfig.getObjectByType(Group.class.getName());
                results.add(LdsProviderUtil.populateGroup(group, ent));
            }

            return results;
        } catch (LdapException | EnterpriseFieldException | EnterpriseConfigurationObjectException e) {
            throw new ProviderException(e.getMessage(), e);
        } finally {
            LdsProviderUtil.releaseLdapConnection(connection);
            long duration = System.currentTimeMillis() - startTiming;
            logger.info(LOGTAG + " - LDAP Group query complete in " + duration + " ms.");
        }
    }

    @Override
    public void create(Group group) throws ProviderException {
        performPreActionSetup();

        long startTiming = System.currentTimeMillis();

        LdapConnection connection = null;
        try {
            connection = LdsProviderUtil.getLdapConnection();

            List<String> elements = new ArrayList<>();
            elements.add("description: " + group.getdescription());
            elements.add("objectClass: " + "group");
            elements.add("objectClass: " + "top");
            // add additional objectClass beyond the mandatory
            group.getobjectClass().stream()
                    .filter(v -> (!v.equals("group") && !v.equals("top")))
                    .forEach(v -> elements.add("objectClass: " + v));

            Entry newEntry = new DefaultEntry(group.getdistinguishedName(), elements.toArray(new Object[elements.size()]));
            connection.add(newEntry);
        } catch (LdapException e) {
            throw new ProviderException(e.getMessage(), e);
        } finally {
            LdsProviderUtil.releaseLdapConnection(connection);
            long duration = System.currentTimeMillis() - startTiming;
            logger.info(LOGTAG + " - LDAP Group create complete in " + duration + " ms.");
        }
    }

    @Override
    public void update(Group group, Group baseline) throws ProviderException {
        performPreActionSetup();

        long startTiming = System.currentTimeMillis();

        List<Modification> mods = new ArrayList<>();
        LdsProviderUtil.determineMod("description", group.getdescription(), baseline.getdescription(), mods);
        LdsProviderUtil.determineMod(false, "objectClass", group.getobjectClass(), baseline.getobjectClass(), mods);

        LdapConnection connection = null;
        try {
            connection = LdsProviderUtil.getLdapConnection();
            connection.modify(group.getdistinguishedName(), mods.toArray(new Modification[mods.size()]));
        } catch (LdapException e) {
            throw new ProviderException(e.getMessage(), e);
        } finally {
            LdsProviderUtil.releaseLdapConnection(connection);
            long duration = System.currentTimeMillis() - startTiming;
            logger.info(LOGTAG + " - LDAP Group update complete in " + duration + " ms.");
        }
    }

    @Override
    public void delete(Group group) throws ProviderException {
        performPreActionSetup();

        long startTiming = System.currentTimeMillis();

        LdapConnection connection = null;
        try {
            connection = LdsProviderUtil.getLdapConnection();
            connection.delete(group.getdistinguishedName());
        } catch (LdapException e) {
            throw new ProviderException(e.getMessage(), e);
        } finally {
            LdsProviderUtil.releaseLdapConnection(connection);
            long duration = System.currentTimeMillis() - startTiming;
            logger.info(LOGTAG + " - LDAP Group delete complete in " + duration + " ms.");
        }
    }


    private void performPreActionSetup() throws ProviderException {
        logger.info(LOGTAG + " - Refreshing local variables ...");
        refreshLocalVariables();
        logger.info(LOGTAG + " - Finished refreshing local variables");
    }

    /**
     * Reset all relevant local variables to account for any config doc
     * changes that may have occurred since last time provider was used.
     *
     * @throws ProviderException on error
     */
    private void refreshLocalVariables() throws ProviderException {
        // do this here so config doc changes will be picked up without a service bounce
        LdsProviderUtil.setProviderProperties(this, appConfig, PROVIDER_PROPERTIES);

        // if the LDAP connection properties changes then create a new pool
        String previousLdapHost = ldapHost;
        int previousLdapPort = ldapPort;
        String previousAdminDn = adminDn;
        String previousAdminPassword = adminPassword;

        ldapHost = getProperties().getProperty(PROP_LDAP_HOST, ldapHost);
        ldapPort = Integer.parseInt(getProperties().getProperty(PROP_LDAP_PORT, String.valueOf(ldapPort)));
        adminDn = getProperties().getProperty(PROP_LDAP_ADMIN_DN, adminDn);
        adminPassword = getProperties().getProperty(PROP_LDAP_ADMIN_PASSWORD, adminPassword);

        if (!ldapHost.equals(previousLdapHost)
                || ldapPort != previousLdapPort
                || !adminDn.equals(previousAdminDn)
                || !adminPassword.equals(previousAdminPassword)) {
            LdsProviderUtil.closeLdapPool();
            LdsProviderUtil.initLdapPool(ldapHost, ldapPort, adminDn, adminPassword);
        }
    }
}
