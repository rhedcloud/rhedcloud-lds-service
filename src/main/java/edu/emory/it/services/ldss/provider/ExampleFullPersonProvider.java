package edu.emory.it.services.ldss.provider;

import edu.emory.moa.jmsobjects.identity.v2_0.FullPerson;
import edu.emory.moa.jmsobjects.identity.v2_0.Person;
import edu.emory.moa.objects.resources.v2_0.FullPersonQuerySpecification;
import edu.emory.moa.objects.resources.v2_0.PersonalName;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;

import java.util.Collections;
import java.util.List;

public class ExampleFullPersonProvider implements FullPersonProvider {
    private AppConfig appConfig;

    @Override
    public void init(AppConfig aConfig) throws ProviderException {
        if (aConfig == null)
            throw new ProviderException("Must have an AppConfig");
        appConfig = aConfig;
    }

    @Override
    public List<FullPerson> query(FullPersonQuerySpecification aqs) throws ProviderException, DirectorySearchException {
        FullPerson fp;
        try {
            fp = (FullPerson) appConfig.getObjectByType(FullPerson.class.getName());
        }
        catch (EnterpriseConfigurationObjectException e) {
            throw new ProviderException(e.getMessage());
        }

        try {
            Person person = fp.newPerson();
            PersonalName personalName = person.newPersonalName();

            personalName.setType("Directory");
            personalName.setFirstName("jenny");
            personalName.setLastName("jenny");
            personalName.setCompositeName("jenny jenny");

            person.setPublicId("P8675309");
            person.setPrsni("00000");
            person.setPersonalName(personalName);

            fp.setPublicId("P8675309");
            fp.setPerson(person);
        }
        catch (EnterpriseFieldException e) {
            throw new ProviderException(e.getMessage());
        }

        return Collections.singletonList(fp);
    }
}
