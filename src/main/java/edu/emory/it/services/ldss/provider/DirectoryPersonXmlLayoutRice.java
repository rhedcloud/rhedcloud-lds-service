package edu.emory.it.services.ldss.provider;

import edu.emory.moa.jmsobjects.identity.v1_0.DirectoryPerson;
import edu.emory.moa.objects.resources.v1_0.Email;
import org.jdom.Document;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.layouts.EnterpriseLayoutException;
import org.openeai.layouts.EnterpriseLayoutManager;
import org.openeai.layouts.EnterpriseLayoutManagerImpl;
import org.openeai.moa.XmlEnterpriseObject;

import java.io.Serializable;
import java.util.Map;

public class DirectoryPersonXmlLayoutRice extends EnterpriseLayoutManagerImpl implements EnterpriseLayoutManager, Serializable {
    public void init(String layoutManagerName, Document layoutDoc) throws EnterpriseLayoutException {
        super.init(layoutManagerName, layoutDoc);
    }

    @Override
    public void buildObjectFromInput(Object input, XmlEnterpriseObject xeo) throws EnterpriseLayoutException {
        @SuppressWarnings("unchecked")
        Map<String, String> pMap = (Map<String, String>) input;
        DirectoryPerson mydp = (DirectoryPerson) xeo;

        try {
            // required fields
            mydp.setFullName(pMap.get("givenName") + " " + pMap.get("sn"));
            mydp.setKey(pMap.get("uid"));
            if (pMap.containsKey("eduPersonPrimaryAffiliation") && !pMap.get("eduPersonPrimaryAffiliation").equals("")) {
                mydp.setType(pMap.get("eduPersonPrimaryAffiliation"));
            } else {
                mydp.setType("Rice");
            }

            // optional fields
            if (pMap.containsKey("givenName"))
                mydp.setFirstMiddle(pMap.get("givenName"));
            if (pMap.containsKey("sn"))
                mydp.setLastName(pMap.get("sn"));
            if (pMap.containsKey("mail") && !pMap.get("mail").equals("")) {
                Email em = mydp.newEmail();
                em.setEmailAddress(pMap.get("mail"));
                em.setType("Primary");
                mydp.setEmail(em);
            }
            if (pMap.containsKey("postalAddress"))
                mydp.setMailStop(pMap.get("postalAddress"));
            if (pMap.containsKey("telephoneNumber"))
                mydp.setDirectoryPhone(pMap.get("telephoneNumber"));
            else if (pMap.containsKey("ricePermPhone"))
                mydp.setDirectoryPhone(pMap.get("ricePermPhone"));
            else if (pMap.containsKey("riceDisplayPhone"))
                mydp.setDirectoryPhone(pMap.get("riceDisplayPhone"));
            if (pMap.containsKey("physicalDeliveryOfficeName"))
                mydp.setDirectoryLocation(pMap.get("physicalDeliveryOfficeName"));
            if (pMap.containsKey("riceCollege"))
                mydp.setDepartmentName(pMap.get("riceCollege"));
            if (pMap.containsKey("riceCurriculum"))
                mydp.setSchoolDivision(pMap.get("riceCurriculum"));
            if (pMap.containsKey("title"))
                mydp.setTitle(pMap.get("title"));

            // no mapping for
            //mydp.setFax
            //mydp.setStudentPhone
            //mydp.setSuffix
        }
        catch (EnterpriseFieldException e) {
            String errMsg = "Error filling DirectoryPerson object from map. The exception is: " + e.getMessage();
            throw new EnterpriseLayoutException(errMsg, e);
        }
    }

    @Override
    public Object buildOutputFromObject(XmlEnterpriseObject xeo) throws EnterpriseLayoutException {
        return null;
    }

    @Override
    public Object buildOutputFromObject(XmlEnterpriseObject xeo, String appName) throws EnterpriseLayoutException {
        setTargetAppName(appName);
        return buildOutputFromObject(xeo);
    }
}
