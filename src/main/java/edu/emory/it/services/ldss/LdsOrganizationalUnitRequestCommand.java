package edu.emory.it.services.ldss;

import com.openii.openeai.commands.MessageMetaData;
import edu.emory.it.services.ldss.provider.OrganizationalUnitProvider;
import edu.emory.it.services.ldss.provider.ProviderException;
import edu.emory.moa.jmsobjects.lightweightdirectoryservices.v1_0.OrganizationalUnit;
import edu.emory.moa.objects.resources.v1_0.OrganizationalUnitQuerySpecification;
import org.jdom.Document;
import org.jdom.Element;
import org.openeai.config.CommandConfig;
import org.openeai.jms.consumer.commands.CommandException;
import org.openeai.jms.consumer.commands.RequestCommand;
import org.openeai.layouts.EnterpriseLayoutException;

import java.util.List;

/**
 * Command that handles Organizational Unit requests for the RHEDcloud Lightweight Directory Services Service.
 */
public class LdsOrganizationalUnitRequestCommand extends LdsRequestCommand implements RequestCommand {

    private static final String PROVIDER_CLASS_NAME = "ldsOrganizationalUnitProviderClassName";

    private OrganizationalUnitProvider provider;

    public LdsOrganizationalUnitRequestCommand(CommandConfig cConfig) throws InstantiationException {
        super(cConfig);

        logger.info(getLogtag() + " Initializing");

        // set the initial provider, this may change when/if the configuration is changed during execute
        provider = initializeProvider();
    }

    @Override
    String handleQuery(MessageMetaData mmd) throws CommandException {
        // retrieve a message object from AppConfig and populate it with data from the incoming message
        OrganizationalUnitQuerySpecification querySpec = (OrganizationalUnitQuerySpecification) retrieveAndBuildObject("Query Data",
                mmd.getQueryObject(), mmd.getQueryObjectName(), (Element) mmd.getQueryObjects().get(0));

        // let the configured provider handle the query
        try {
            List<OrganizationalUnit> results = provider.query(querySpec);

            Document replyDoc = getReplyDoc(mmd.getMessageAction(), mmd.getMessageObject(), mmd.getMessageRelease());
            if (results == null || results.isEmpty()) {
                logger.info(getLogtag() + " NULL query response ... returning empty reply");

                // remove the contents of the DataArea element from the primed Provide-Reply document
                // to give an empty reply
                replyDoc.getRootElement().getChild(DATA_AREA).removeChildren(mmd.getMessageObject());
            }
            else {
                logger.info(getLogtag() + " Adding " + mmd.getMessageObject() + " object to the Provide-Reply document.");

                // remove the contents of the DataArea element from the primed Provide-Reply document
                // before adding our output Element
                replyDoc.getRootElement().getChild(DATA_AREA).removeChildren(mmd.getMessageObject());

                for (OrganizationalUnit ou : results) {
                    ou.setOutputLayoutManager(ou.getOutputLayoutManager("xml"));
                    Element dataAreaContent = (Element) ou.buildOutputFromObject();
                    replyDoc.getRootElement().getChild(DATA_AREA).addContent(dataAreaContent);
                }
            }

            // return the populated Provide-Reply document...
            return buildReplyDocument(mmd.getControlArea(), replyDoc);
        }
        catch (ProviderException e) {
            String errDescription = "Exception occurred querying the '" + mmd.getMessageObject() + "' object.";
            return createErrorResponseReply(mmd, "application", "LDSS-2001", errDescription, e);
        }
        catch (EnterpriseLayoutException e) {
            String errDescription = "Exception occurred creating the '" + mmd.getMessageObject()
                    + "' object(s) from the data passed back from the provider.";
            return createErrorResponseReply(mmd, "application", "LDSS-2002", errDescription, e);
        }
    }

    @Override
    String handleCreate(MessageMetaData mmd) throws CommandException {
        // retrieve a message object from AppConfig and populate it with data from the incoming message
        OrganizationalUnit ou = (OrganizationalUnit) retrieveAndBuildObject("Create Data",
                mmd.getMessageObject(), mmd.getMessageObjectName(), mmd.getData());

        try {
            provider.create(ou);
            publishSync(mmd.getMessageObject(), mmd.getMessageAction(), ou, null);

            Document replyDoc = getReplyDoc(mmd.getMessageAction(), mmd.getMessageObject(), mmd.getMessageRelease());
            return buildReplyDocument(mmd.getControlArea(), replyDoc);
        }
        catch (ProviderException e) {
            String errDescription = "Exception occurred creating the '" + mmd.getMessageObject() + "' object.";
            return createErrorResponseReply(mmd, "application", "LDSS-2003", errDescription, e);
        }
    }

    @Override
    String handleUpdate(MessageMetaData mmd) throws CommandException {
        // retrieve a message object from AppConfig and populate it with data from the incoming message
        OrganizationalUnit ou = (OrganizationalUnit) retrieveAndBuildObject("Update Data",
                mmd.getMessageObject(), mmd.getMessageObjectName(), mmd.getData());
        OrganizationalUnit baseline = (OrganizationalUnit) retrieveAndBuildObject("Baseline Data",
                mmd.getMessageObject(), mmd.getMessageObjectName(), mmd.getBaselineData());

        try {
            provider.update(ou, baseline);
            publishSync(mmd.getMessageObject(), mmd.getMessageAction(), ou, baseline);

            Document replyDoc = getReplyDoc(mmd.getMessageAction(), mmd.getMessageObject(), mmd.getMessageRelease());
            return buildReplyDocument(mmd.getControlArea(), replyDoc);
        }
        catch (ProviderException e) {
            String errDescription = "Exception occurred updating the '" + mmd.getMessageObject() + "' object.";
            return createErrorResponseReply(mmd, "application", "LDSS-2004", errDescription, e);
        }
    }

    @Override
    String handleDelete(MessageMetaData mmd) throws CommandException {
        // retrieve a message object from AppConfig and populate it with data from the incoming message
        OrganizationalUnit ou = (OrganizationalUnit) retrieveAndBuildObject("Delete Data",
                mmd.getMessageObject(), mmd.getMessageObjectName(), mmd.getData());

        try {
            provider.delete(ou);
            publishSync(mmd.getMessageObject(), mmd.getMessageAction(), ou, null);

            Document replyDoc = getReplyDoc(mmd.getMessageAction(), mmd.getMessageObject(), mmd.getMessageRelease());
            return buildReplyDocument(mmd.getControlArea(), replyDoc);
        }
        catch (ProviderException e) {
            String errDescription = "Exception occurred deleting the '" + mmd.getMessageObject() + "' object.";
            return createErrorResponseReply(mmd, "application", "LDSS-2005", errDescription, e);
        }
    }

    @Override
    String getLogtag() { return "[LdsOrganizationalUnitRequestCommand]"; }
    @Override
    String getRequiredMessageObject() { return "OrganizationalUnit"; }

    private OrganizationalUnitProvider initializeProvider() throws InstantiationException {
        String providerClassName = getProperties().getProperty(PROVIDER_CLASS_NAME);
        if (providerClassName == null || providerClassName.equals("")) {
            String errMsg = getLogtag() + " No " + PROVIDER_CLASS_NAME + " property specified. Can't continue.";
            logger.fatal(errMsg);
            throw new InstantiationException(errMsg);
        }

        try {
            logger.info(getLogtag() + " Getting provider class for name: " + providerClassName);
            Class<?> providerClass = Class.forName(providerClassName);
            OrganizationalUnitProvider providerInstance = (OrganizationalUnitProvider) providerClass.newInstance();
            providerInstance.init(getAppConfig());
            logger.info(getLogtag() + " Initialization complete.");

            return providerInstance;
        }
        catch (ClassNotFoundException e) {
            String errMsg = getLogtag() + " Class named " + providerClassName + "not found on the classpath.  The exception is: " + e.getMessage();
            logger.fatal(errMsg);
            throw new InstantiationException(errMsg);
        }
        catch (IllegalAccessException e) {
            String errMsg = getLogtag() + " An error occurred getting a class for name: " + providerClassName + ". The exception is: " + e.getMessage();
            logger.fatal(errMsg);
            throw new InstantiationException(errMsg);
        }
        catch (ProviderException e) {
            String errMsg = getLogtag() + " An error occurred initializing the provider " + providerClassName + ". The exception is: " + e.getMessage();
            logger.fatal(errMsg);
            throw new InstantiationException(errMsg);
        }
    }
}
