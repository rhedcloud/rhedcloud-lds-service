package edu.emory.it.services.ldss.provider;

import java.util.*;

/**
 * Compares two Maps of String/String in order given by an
 * array of keys, with optional array indicating if increasing or decreasing
 * for each key.
 * The keys are padded on the right as needed to make them
 * the same length before comparing them as strings.
 * Null precedes all other values.
 * Unspecified order defaults to ascending.
 */
public class HashMapComparator implements Comparator<Map<String, String>> {
    private final String[] key;
    private final int[] order;


    /**
     * Create comparator with specified
     * keys and the order, ascending or descending, for each key.
     * If the map does not contain a key, then its value is null.
     * Null precedes all other values.
     * Unspecified order defaults to ascending.
     *
     * @param key   - array of keys in the order to check
     * @param order - array of <code>int</code> with 1 meaning ascending or -1 meaning descending.
     *              If this array is shorter than the key array, then 1 is
     *              assumed for the missing ones. Use <code>int[] order={}</code> to default
     *              to all ascending.
     */
    public HashMapComparator(String[] key, int[] order) {
        this.key = key;
        this.order = new int[key.length];
        System.arraycopy(order, 0, this.order, 0, order.length);
        for (int i = order.length; i < key.length; i++) {
            this.order[i] = 1;
        }
    }

    /**
     * pads given string with " " to the right to the given length (n).
     * Author http://www.rgagnon.com/javadetails/java-0448.html
     *
     * @param s string to be padded
     * @param n int pad to this length
     * @return padded string of length n
     */
    private static String padRight(String s, int n) {
        return String.format("%1$-" + n + "s", s);
    }

    /**
     * Compares two Maps of String/String objects ascending or descending on given keys
     * using alphabetic ordering in which the keys are padded on the right as needed to make them
     * the same length before comparing them as strings.
     *
     * @param m1 object to compare.
     * @param m2 object to compare.
     * @return a negative integer, zero, or a positive integer as the
     *         first argument is less than, equal to, or greater than the second
     */
    public int compare(Map<String, String> m1, Map<String, String> m2) {
        boolean m1HasKey, m2HasKey;
        boolean m1NullVal, m2NullVal;
        int result = 0;
        int compareLen;
        for (int i = 0; i < key.length && result == 0; i++) {
            m1HasKey = m1.containsKey(key[i]);
            m2HasKey = m2.containsKey(key[i]);
            if (m1HasKey && m2HasKey) {
                // compareTo(arg) will blow if arg is null or has value null
                m1NullVal = (m1.get(key[i]) == null);
                m2NullVal = (m2.get(key[i]) == null);
                if (!m1NullVal && !m2NullVal) {
                    compareLen = Math.max(m1.get(key[i]).length(), m2.get(key[i]).length());
                    result = order[i] * (padRight(m1.get(key[i]), compareLen)).compareTo(padRight(m2.get(key[i]), compareLen));
                } else if (m1NullVal && !m2NullVal) {
                    result = -1;
                } else if (!m1NullVal && m2NullVal) {
                    result = 1;
                } else if (m1NullVal && m2NullVal) {
                    result = 0;
                }
            } else if (!m1HasKey && m2HasKey) {
                result = -1;
            } else if (m1HasKey && !m2HasKey) {
                result = 1;
            } else if (!m1HasKey && !m2HasKey) { // for completeness
                result = 0;
            }
        }
        return result;
    }
}


