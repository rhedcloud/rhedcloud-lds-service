package edu.emory.it.services.ldss.provider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.naming.LimitExceededException;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Implements Directory basic search and search by key, returning an List
 * of Maps of String/String of attributes and values of people that match.
 * Each Map maps LDAP attribute names to their values.
 * The List is sorted according to sortKeys.
 * DirectorySearchLDS does not throw any exceptions. Instead it sets an error code and an error message.
 */
public class DirectorySearchLDS {
    protected static Logger logger = LogManager
            .getLogger("edu.emory.it.services.ldss");
    private static final String LOGTAG = "[DirectorySearchLDS] ";

    private final DirContext ctx;
    private String ldapSearchBase;
    private String returningAttributes;
    private String primaryKey;
    private String sortKeys;
    private String basicSearchAttributes;
    private String searchRestriction;

    /** Indicates that there were no matches to the filter. */
    public static final int NO_MATCHES = 1;
    /** Indicates that the number of matches was greater than the maximum allowed, which is 100. */
    public static final int TOO_MANY = 2;
    /** Indicates that some java exception was thrown. */
    public static final int THREW_EXCEPTION = 3;

    private int errorCode;
    private String errorMessage;


    /**
     * Creates a DirectorySearchLDS object with methods to
     * set parameters, do searches
     * and retrieve the results, including error code and error message.
     *
     * @param ctx DirContext with INITIAL_CONTEXT_FACTORY,
     * PROVIDER_URL, SECURITY_PRINCIPAL (username), and
     * SECURITY_CREDENTIALS (password)
     */
    public DirectorySearchLDS(DirContext ctx) {
        this.ctx = ctx;
    }

    /**
     * Set the value of ldapSearchBase.
     * 
     * @param ldapSearchBase search base
     */
    public void setLdapSearchBase(String ldapSearchBase) {
        this.ldapSearchBase = ldapSearchBase;
    }

    /**
     * Set the value of returningAttributes.
     *
     * @param returningAttributes attributes to return from search
     */
    public void setReturningAttributes(String returningAttributes) {
        this.returningAttributes = returningAttributes;
    }

    /**
     * Set the value of primaryKey.
     *
     * @param primaryKey primary key attribute
     */
    public void setPrimaryKey(String primaryKey) {
        this.primaryKey = primaryKey;
    }

    /**
     * Set the value of sortKeys.
     *
     * @param sortKeys attributes to sort on
     */
    public void setSortKeys(String sortKeys) {
        this.sortKeys = sortKeys;
    }

    /**
     * Set the value of basicSearchAttributes.
     *
     * @param basicSearchAttributes attributes to search on
     */
    public void setBasicSearchAttributes(String basicSearchAttributes) {
        this.basicSearchAttributes = basicSearchAttributes;
    }

    /**
     * Set the value of searchRestriction.
     *
     * @param searchRestriction attributes to sort on
     */
    public void setSearchRestriction(String searchRestriction) {
        this.searchRestriction = searchRestriction;
    }

    /**
     * Get the value of errorCode.
     * 
     * @return the int value of errorCode
     */
    public int getErrorCode() {
        return errorCode;
    }

    /**
     * Get the value of errorMessage. Any exceptions that are thrown
     * are documented here.
     * 
     * @return the String value of errorMessage
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * Sanitizes given search string, constructs filter, runs basic search, and returns query result.
     *
     * @param searchString String
     * @return List of Maps of String/String of matches. Sets errorCode and errorMessage.
     */
    public List<Map<String, String>> doBasic(String searchString) {
        String searchFilter = "";

        searchString = fixSearchString(searchString);

        if (!searchString.equals("")) {
            searchFilter += "(|";
            for (String att : basicSearchAttributes.split(",")) {
                searchFilter += "(";
                searchFilter += att + "=*" + searchString + "*";
                searchFilter += ")";
            }
            searchFilter += ")";

            searchFilter = applySearchRestrictions(searchFilter);
        }

        return ldapQuery(searchFilter);
    }

    /**
     * Gets entry for given key.
     *
     * @param key  String that is the directory key of entry to return
     * @return List of Maps of String/String of the entry with the given key
     * or empty list if not found. Sets errorCode and errorMessage.
     */
    public List<Map<String, String>> doGetEntry(String key) {
        key = key.replaceAll("[^A-Za-z0-9]", "");
        String searchFilter = "(" + primaryKey + "=" + key + ")";
        searchFilter = applySearchRestrictions(searchFilter);
        return ldapQuery(searchFilter);
    }

    /**
     * Fixes the search value by handling special characters
     * such as removing backslashes and escaping parentheses.
     *
     * @param searchString String containing value to fix
     * @return String containing fixed search string
     */
    private String fixSearchString(String searchString) {
        if (searchString == null)
            return "";

        /* Remove all initial and final white space and asterisks
           and replace all white space between words with a single asterisk */
        searchString = searchString.replaceAll("\\*", " ");
        searchString = searchString.replaceAll("\\\\", " ");
        searchString = searchString.replaceAll("\\s{2,}", " ");
        searchString = searchString.trim();
        searchString = searchString.replaceAll(" ", "\\*");

        // Thwart LDAP filter injections, such as: xx)(eodentry=00000001
        // Also see https://en.wikipedia.org/wiki/LDAP_injection
        searchString = searchString.replaceAll("\\(", "\\\\28");
        searchString = searchString.replaceAll("\\)", "\\\\29");

        // Thwart search strings that attempt to generate buffer overflow by being overly long.
        searchString = searchString.substring(0, Math.min(80, searchString.length()));

        return searchString;
    }

    /**
     * Some sites place restrictions on the set of data that can be returned.
     * This is a search clause that will be AND'd to the search filter.
     *
     * @param searchFilter search filter
     * @return search filter with applicable restrictions
     */
    private String applySearchRestrictions(String searchFilter) {
        if (searchRestriction == null || searchRestriction.isEmpty())
            return searchFilter;
        return "(&" + searchFilter + searchRestriction + ")";
    }

    /**
     * Runs LDAP search.
     *
     * @param searchFilter String containing filter to use
     * @return List of Maps of String/String of matches.
     * Sets errorCode and errorMessage.
     * Returns empty list if no matches.
     */
    private List<Map<String, String>> ldapQuery(String searchFilter) {
        errorMessage = "";
        errorCode = 0;
        searchFilter = searchFilter.trim();

        // Initialize result to empty list in case query throws an error or filter is null.
        List<Map<String, String>> result = new ArrayList<>();

        // Search must have a non-empty filter or else overflows heap space
        if (!searchFilter.equals("")) {
            // max records returned is globally set in some LDAP servers to 102
            long countLimit = 100;
            String ldapScope = "sub";  // SearchControls.SUBTREE_SCOPE
            String delimiter = ";";

            try {
                logger.debug(LOGTAG + " SearchFilter = " + searchFilter);
                logger.debug(LOGTAG + "   SearchBase = " + ldapSearchBase);

                result = searchToListOfMaps(ctx, ldapSearchBase, searchFilter, ldapScope, returningAttributes, delimiter, countLimit);

                if (result.size() > 1) {
                    // Sort the list on lastFirst
                    String[] SORT_KEY = sortKeys.split(",");
                    int[] SORT_ORDER = {}; // default ascending
                    result.sort(new HashMapComparator(SORT_KEY, SORT_ORDER));
                }
            }
            catch (LimitExceededException e) {
                // result is unchanged and has size of zero
                errorCode = TOO_MANY;
                errorMessage = "More than " + countLimit + " matches found. Please make your search more specific.";
            }
            catch (Exception e) {
                errorCode = THREW_EXCEPTION;
                errorMessage = e.getMessage();
            }
        }
        if (result.size() == 0 && errorCode == 0) {
            errorCode = NO_MATCHES;
            errorMessage = "No matches were found for your search.";
        }
        return result;
    }

    /**
     * Search LDAP connection and returns results as NamingEnumeration of SearchResult.
     *
     * @param ctx DirContext: LDAP connection to use
     * @param searchBase String: Where to start the search
     * @param searchFilter String: filter to use
     * @param searchScope String: base, one, sub
     * @param returningAttributesCsv String: comma separated list of attributes to return
     * @param countLimit  (long) maximum number of rows to return if greater or equal to 0, else no limit.
     * @throws NamingException naming exception
     * @throws LimitExceededException  if returns more than countLimit
     * @return Matching entries as NamingEnumeration of SearchResult.
     * If a non-existent attribute is requested, it is returned with value empty string.
     */
    public static NamingEnumeration<SearchResult> search(DirContext ctx,
                                                         String searchBase, String searchFilter, String searchScope,
                                                         String returningAttributesCsv, long countLimit)
            throws NamingException, LimitExceededException {

        SearchControls searchControls = new SearchControls();
        switch (searchScope) {
            case "base":
                searchControls.setSearchScope(SearchControls.OBJECT_SCOPE);
                break;
            case "one":
                searchControls.setSearchScope(SearchControls.ONELEVEL_SCOPE);
                break;
            case "sub":
                searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
                break;
        }
        if (returningAttributesCsv != null)
            searchControls.setReturningAttributes(returningAttributesCsv.split(","));
        if (countLimit >= 0)
            searchControls.setCountLimit(countLimit);
        return ctx.search(searchBase, searchFilter, searchControls);
    }

    /**
     * Search LDAP connection and returns result as List of Maps of String/String.
     *
     * @param ctx DirContext: LDAP connection to use
     * @param searchBase String: where to start search
     * @param searchFilter String:
     * @param searchScope String: base, one, sub
     * @param returningAttributesCsv String: comma-delimited attribute names to return
     * @param delimiter String: use as value separator if attribute has more than one value
     * @param countLimit  (long) maximum number of rows to return if greater or equal to 0, else no limit.
     * @return Matching entries as List of Maps of String/String
     * whose keys are the given attribute names in given case and whose map
     * values are the join of the attribute values using the given delimiter.
     * If a non-existent attribute is requested, it is returned with value empty string.
     * @throws NamingException naming exception
     * @throws LimitExceededException  if returns more than countLimit
     */
    public static List<Map<String, String>> searchToListOfMaps(DirContext ctx,
                                                               String searchBase, String searchFilter, String searchScope,
                                                               String returningAttributesCsv, String delimiter, long countLimit)
            throws NamingException, LimitExceededException {

        NamingEnumeration<SearchResult> answer = search(ctx, searchBase, searchFilter, searchScope, returningAttributesCsv, countLimit);
        List<Map<String, String>> result = new ArrayList<>();
        List<String> attrValList = new ArrayList<>();
        while (answer.hasMore()) {
            // each search result contains name, object, class name, and Attributes
            SearchResult sr = answer.next();
            Attributes srAttrs = sr.getAttributes();
            // Must get new list to hold values of attr for each result
            Map<String, String> attrsMap = new HashMap<>();
            NamingEnumeration<? extends Attribute> allAttrs = srAttrs.getAll();
            while (allAttrs.hasMore()) {
                try {
                    Attribute srAttr = allAttrs.next();
                    attrValList.clear(); // to hold values of an attribute
                    for (NamingEnumeration<?> attrValues = srAttr.getAll(); attrValues.hasMore();) {
                        String attrValue = (String) attrValues.next();
                        attrValList.add(attrValue);
                    }
                    attrsMap.put(srAttr.getID(), String.join(delimiter, attrValList));
                }
                catch (NullPointerException e) {
                    // ignore
                }
            }
            result.add(attrsMap);
        }
        return result;
    }
}
