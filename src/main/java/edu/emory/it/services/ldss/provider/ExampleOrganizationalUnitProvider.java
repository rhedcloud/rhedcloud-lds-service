package edu.emory.it.services.ldss.provider;

import edu.emory.moa.jmsobjects.lightweightdirectoryservices.v1_0.OrganizationalUnit;
import edu.emory.moa.objects.resources.v1_0.OrganizationalUnitQuerySpecification;
import org.apache.directory.api.ldap.model.exception.LdapEntryAlreadyExistsException;
import org.apache.directory.api.ldap.model.exception.LdapNoSuchObjectException;
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExampleOrganizationalUnitProvider extends OpenEaiObject implements OrganizationalUnitProvider {
    private static final Map<String, List<OrganizationalUnit>> directory = new HashMap<>();

    @Override
    public void init(AppConfig aConfig) throws ProviderException {
    }

    @Override
    public List<OrganizationalUnit> query(OrganizationalUnitQuerySpecification qs) throws ProviderException {
        return directory.get(qs.getdistinguishedName());
    }

    @Override
    public void create(OrganizationalUnit ou) throws ProviderException {
        if (directory.containsKey(ou.getdistinguishedName())) {
            LdapEntryAlreadyExistsException e = new LdapEntryAlreadyExistsException(
                    "00002071: UpdErr: DSID-0305038D, problem 6005 (ENTRY_EXISTS), data 0\n\u0000");
            throw new ProviderException(e.getMessage(), e);
        }
        directory.computeIfAbsent(ou.getdistinguishedName(), k -> new ArrayList<>()).add(ou);
    }

    @Override
    public void update(OrganizationalUnit ou, OrganizationalUnit baseline) throws ProviderException {
        if (!directory.containsKey(ou.getdistinguishedName())) {
            LdapNoSuchObjectException e = new LdapNoSuchObjectException(
                    "0000208D: NameErr: DSID-03100238, problem 2001 (NO_OBJECT), data 0, best match of:\n\u0000");
            throw new ProviderException(e.getMessage(), e);
        }
        directory.remove(ou.getdistinguishedName());
        directory.computeIfAbsent(ou.getdistinguishedName(), k -> new ArrayList<>()).add(ou);
    }

    @Override
    public void delete(OrganizationalUnit ou) throws ProviderException {
        if (!directory.containsKey(ou.getdistinguishedName())) {
            LdapNoSuchObjectException e = new LdapNoSuchObjectException(
                    "0000208D: NameErr: DSID-03100238, problem 2001 (NO_OBJECT), data 0, best match of:\n\u0000");
            throw new ProviderException(e.getMessage(), e);
        }
        directory.remove(ou.getdistinguishedName());
    }
}
