package edu.emory.it.services.ldss.provider;

import edu.emory.moa.jmsobjects.lightweightdirectoryservices.v1_0.Group;
import edu.emory.moa.objects.resources.v1_0.GroupQuerySpecification;
import org.apache.directory.api.ldap.model.exception.LdapEntryAlreadyExistsException;
import org.apache.directory.api.ldap.model.exception.LdapNoSuchObjectException;
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExampleGroupProvider extends OpenEaiObject implements GroupProvider {
    private static final Map<String, List<Group>> directory = new HashMap<>();

    @Override
    public void init(AppConfig aConfig) throws ProviderException {
    }

    @Override
    public List<Group> query(GroupQuerySpecification qs) throws ProviderException {
        return directory.get(qs.getdistinguishedName());
    }

    @Override
    public void create(Group group) throws ProviderException {
        if (directory.containsKey(group.getdistinguishedName())) {
            LdapEntryAlreadyExistsException e = new LdapEntryAlreadyExistsException(
                    "00002071: UpdErr: DSID-0305038D, problem 6005 (ENTRY_EXISTS), data 0\n\u0000");
            throw new ProviderException(e.getMessage(), e);
        }
        directory.computeIfAbsent(group.getdistinguishedName(), k -> new ArrayList<>()).add(group);
    }

    @Override
    public void update(Group group, Group baseline) throws ProviderException {
        if (!directory.containsKey(group.getdistinguishedName())) {
            LdapNoSuchObjectException e = new LdapNoSuchObjectException(
                    "0000208D: NameErr: DSID-03100238, problem 2001 (NO_OBJECT), data 0, best match of:\n\u0000");
            throw new ProviderException(e.getMessage(), e);
        }
        directory.remove(group.getdistinguishedName());
        directory.computeIfAbsent(group.getdistinguishedName(), k -> new ArrayList<>()).add(group);
    }

    @Override
    public void delete(Group group) throws ProviderException {
        if (!directory.containsKey(group.getdistinguishedName())) {
            LdapNoSuchObjectException e = new LdapNoSuchObjectException(
                    "0000208D: NameErr: DSID-03100238, problem 2001 (NO_OBJECT), data 0, best match of:\n\u0000");
            throw new ProviderException(e.getMessage(), e);
        }
        directory.remove(group.getdistinguishedName());
    }
}
