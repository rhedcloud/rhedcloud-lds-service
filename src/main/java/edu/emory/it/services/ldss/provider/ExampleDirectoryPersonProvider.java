package edu.emory.it.services.ldss.provider;

import edu.emory.moa.jmsobjects.identity.v1_0.DirectoryPerson;
import edu.emory.moa.objects.resources.v1_0.DirectoryPersonQuerySpecification;
import edu.emory.moa.objects.resources.v1_0.Email;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;

import java.util.Collections;
import java.util.List;

public class ExampleDirectoryPersonProvider implements DirectoryPersonProvider {
    private AppConfig appConfig;

    @Override
    public void init(AppConfig aConfig) throws ProviderException {
        if (aConfig == null)
            throw new ProviderException("Must have an AppConfig");
        appConfig = aConfig;
    }

    @Override
    public List<DirectoryPerson> query(DirectoryPersonQuerySpecification aqs) throws ProviderException, DirectorySearchException {
        DirectoryPerson dp;
        try {
            dp = (DirectoryPerson) appConfig.getObjectByType(DirectoryPerson.class.getName());
        } catch (EnterpriseConfigurationObjectException e) {
            throw new ProviderException(e.getMessage());
        }

        try {
            dp.setFullName("jenny jenny");
            dp.setStudentPhone("867-5309");
            dp.setKey("P8675309");
            Email em = new Email();
            em.setEmailAddress("jenny@tommy.tutone");
            em.setType("Primary");
            dp.setEmail(em);
            dp.setFirstMiddle("jenny");
            dp.setLastName("jenny");
            dp.setType("Sponsored");
        } catch (EnterpriseFieldException e) {
            throw new ProviderException(e.getMessage());
        }

        return Collections.singletonList(dp);
    }
}
