package edu.emory.it.services.ldss.provider;

import edu.emory.moa.jmsobjects.lightweightdirectoryservices.v1_0.OrganizationalUnit;
import edu.emory.moa.objects.resources.v1_0.OrganizationalUnitQuerySpecification;
import org.openeai.config.AppConfig;

import java.util.List;


public interface OrganizationalUnitProvider {
    /**
     * Initialize the provider.
     *
     * @param aConfig, an AppConfig object with all this provider needs.
     * @throws ProviderException on error.
     */
    void init(AppConfig aConfig) throws ProviderException;

    /**
     * Query for an LDS organizational unit.
     *
     * @param qs the query spec.
     * @return OrganizationalUnit, the query results.
     * @throws ProviderException on error.
     */
    List<OrganizationalUnit> query(OrganizationalUnitQuerySpecification qs) throws ProviderException;

    /**
     * Create an LDS organizational unit.
     *
     * @param ou to create.
     * @throws ProviderException on error.
     */
    void create(OrganizationalUnit ou) throws ProviderException;

    /**
     * Update an LDS organizational unit.
     *
     * @param ou to update.
     * @param baseline original.
     * @throws ProviderException on error.
     */
    void update(OrganizationalUnit ou, OrganizationalUnit baseline) throws ProviderException;

    /**
     * Delete an LDS organizational unit.
     *
     * @param ou to delete.
     * @throws ProviderException on error.
     */
    void delete(OrganizationalUnit ou) throws ProviderException;
}
