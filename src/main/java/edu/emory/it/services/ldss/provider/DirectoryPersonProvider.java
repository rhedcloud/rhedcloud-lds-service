package edu.emory.it.services.ldss.provider;

import java.util.ArrayList;
import java.util.List;

import edu.emory.it.services.ldss.provider.ProviderException;
import edu.emory.moa.jmsobjects.identity.v1_0.DirectoryPerson;
import edu.emory.moa.objects.resources.v1_0.DirectoryPersonQuerySpecification;
import org.openeai.config.AppConfig;

/**
 * Interface for all Directory Person providers.
 */
public interface DirectoryPersonProvider {
    /**
     * @param aConfig an AppConfig object with all this provider needs
     * @throws ProviderException with details of the initialization error
     */
    void init(AppConfig aConfig) throws ProviderException;

    /**
     * @param aqs DirectoryPersonQuerySpecification, the query parameters
     * @return List of DirectoryPerson
     * @throws DirectorySearchException directory search errors
     * @throws ProviderException with details of the error
     */
    List<DirectoryPerson> query(DirectoryPersonQuerySpecification aqs) throws ProviderException, DirectorySearchException;
}