package edu.emory.it.services.ldss.provider;

/**
 * An exception for Directory Person provider errors.
 * The purpose is to allow reporting the error up the chain of callers,
 * given that the directory search itself returns the error code and
 * error message as attributes of the search object. A separate exception
 * was needed to avoid logging inappropriate fatal errors, yet get the
 * error message into the reply. The format of the message is
 * error number followed by a space followed by the text of the error message.
 * The error numbers and their meanings are defined in the DirectorySearch class.
 */
public class DirectorySearchException extends Exception {
    /**
     * Default constructor.
     */
    public DirectorySearchException() {
        super();
    }

    /**
     * Message constructor.
     */
    public DirectorySearchException(String msg) {
        super(msg);
    }

    /**
     * Throwable constructor.
     */
    public DirectorySearchException(String msg, Throwable e) {
        super(msg, e);
    }
}

