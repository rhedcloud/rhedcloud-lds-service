package edu.emory.it.services.ldss.provider;

import edu.emory.moa.jmsobjects.lightweightdirectoryservices.v1_0.Group;
import edu.emory.moa.objects.resources.v1_0.GroupQuerySpecification;
import org.openeai.config.AppConfig;

import java.util.List;


public interface GroupProvider {
    /**
     * Initialize the provider.
     *
     * @param aConfig, an AppConfig object with all this provider needs.
     * @throws ProviderException on error.
     */
    void init(AppConfig aConfig) throws ProviderException;

    /**
     * Query for an LDS group.
     *
     * @param qs the query spec.
     * @return Group, the query results.
     * @throws ProviderException on error.
     */
    List<Group> query(GroupQuerySpecification qs) throws ProviderException;

    /**
     * Create an LDS group.
     *
     * @param group to create.
     * @throws ProviderException on error.
     */
    void create(Group group) throws ProviderException;

    /**
     * Update an LDS group.
     *
     * @param group to update.
     * @param baseline original.
     * @throws ProviderException on error.
     */
    void update(Group group, Group baseline) throws ProviderException;

    /**
     * Delete an LDS group.
     *
     * @param group to delete.
     * @throws ProviderException on error.
     */
    void delete(Group group) throws ProviderException;
}
