package edu.emory.it.services.ldss.provider;

import edu.emory.moa.jmsobjects.identity.v2_0.FullPerson;
import edu.emory.moa.jmsobjects.identity.v2_0.Person;
import edu.emory.moa.objects.resources.v2_0.PersonalName;
import org.jdom.Document;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.layouts.EnterpriseLayoutException;
import org.openeai.layouts.EnterpriseLayoutManager;
import org.openeai.layouts.EnterpriseLayoutManagerImpl;
import org.openeai.moa.XmlEnterpriseObject;

import java.io.Serializable;
import java.util.Map;

public class FullPersonXmlLayoutEmory extends EnterpriseLayoutManagerImpl implements EnterpriseLayoutManager, Serializable {
    public void init(String layoutManagerName, Document layoutDoc) throws EnterpriseLayoutException {
        super.init(layoutManagerName, layoutDoc);
    }

    @Override
    public void buildObjectFromInput(Object input, XmlEnterpriseObject xeo) throws EnterpriseLayoutException {
        @SuppressWarnings("unchecked")
        Map<String, String> pMap = (Map<String, String>) input;
        FullPerson myfp = (FullPerson) xeo;

        try {
            Person person = myfp.newPerson();
            PersonalName personalName = person.newPersonalName();

            personalName.setType("Directory");  // seems like the right choice
            personalName.setFirstName(pMap.get("emoryGivenName"));
            personalName.setLastName(pMap.get("sn"));
            if (pMap.containsKey("fullName"))
                personalName.setCompositeName(pMap.get("fullName"));
            else
                personalName.setCompositeName(pMap.get("emoryGivenName") + " " + pMap.get("sn"));

            person.setPublicId(pMap.get("serialNumber"));
            person.setPrsni("00000");  // required but unknown so make something up
            person.setPersonalName(personalName);

            myfp.setPublicId(pMap.get("serialNumber"));
            myfp.setPerson(person);
        }
        catch (EnterpriseFieldException e) {
            String errMsg = "Error setting FullPerson object from map. The exception is: " + e.getMessage();
            throw new EnterpriseLayoutException(errMsg, e);
        }
    }

    @Override
    public Object buildOutputFromObject(XmlEnterpriseObject xeo) throws EnterpriseLayoutException {
        return null;
    }

    @Override
    public Object buildOutputFromObject(XmlEnterpriseObject xeo, String appName) throws EnterpriseLayoutException {
        setTargetAppName(appName);
        return buildOutputFromObject(xeo);
    }
}
