curl -v \
     --header "Content-Type: text/xml;charset=UTF-8" \
     --header "Accept: gzip,deflate" \
     --header "SOAPAction: http://www.emory.edu/LdsService/OrganizationalUnitQuery" \
     --data @- \
     \
        http://localhost:8080/axis2/services/LdsService << EOF
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                  xmlns:lds="http://www.emory.edu/LdsService/"
                  xmlns:lds1="http://www.emory.edu/lds/">
   <soapenv:Header/>
   <soapenv:Body>
      <lds:OrganizationalUnitQueryRequest>
         <lds1:OrganizationalUnitQuerySpecification>
            <lds1:distinguishedName>OU=999999999998,OU=AWS,DC=emory,DC=edu</lds1:distinguishedName>
         </lds1:OrganizationalUnitQuerySpecification>
      </lds:OrganizationalUnitQueryRequest>
   </soapenv:Body>
</soapenv:Envelope>
EOF
